#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Build and benchmark CMAQ.
### For details see https://bitbucket.org/epa_n2o_project_team/cmaq-build
### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `date`
### * `dirname`
### * `ls`
### * `readlink`
### * `tee`
### * `tr`

### Currently I'm testing this code in a commandline wrapper:
# GIT_ROOT='/project/inf35w/roche/CMAQ-5.0.1/git/old'
# UBER_APPL='CMAQv5_0_1'
# UBER_CFG='CMAQ-BENCHMARK'
# DRIVER_FP="${GIT_ROOT}/CMAQ-build/CMAQ-build_driver.sh"

# BCON_BUILD_DIR="${GIT_ROOT}/BLD_BCON_${UBER_APPL}"
# BCON_RUN_DIR="${GIT_ROOT}/RUN_BCON_${UBER_APPL}"

# ICON_BUILD_DIR="${GIT_ROOT}/BLD_ICON_${UBER_APPL}"
# ICON_RUN_DIR="${GIT_ROOT}/RUN_ICON_${UBER_APPL}"

# CCTM_BUILD_DIR="${GIT_ROOT}/BLD_CCTM_${UBER_APPL}"
# CCTM_RUN_DIR="${GIT_ROOT}/RUN_CCTM_${UBER_APPL}"

# for CMD in \
#   "rm -fr ${BCON_BUILD_DIR}/" \
#   "rm -fr ${BCON_RUN_DIR}/" \
#   "rm -fr ${ICON_BUILD_DIR}/" \
#   "rm -fr ${ICON_RUN_DIR}/" \
#   "rm -fr ${CCTM_BUILD_DIR}/" \
#   "rm -fr ${CCTM_RUN_DIR}/" \
#   "${DRIVER_FP}" \
# ; do
#   echo -e "$ ${CMD}"
#   eval "${CMD}"
# done

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
export LOG_FP="${THIS_DIR}/${LOG_FN}"

### more constants and other envvars setup below in 'function setup_vars'

### common code for build and run
UTILS="${THIS_DIR}/CMAQ_utilities.sh"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### Set main/pilot envvars from our uber.config.cmaq
function setup_vars {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # common code for build and run in UTILS::setup_CMAQ_common_vars
  if [[ -r "${UTILS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} about to 'source' utilities file='${UTILS}'" 2>&1 | tee -a "${LOG_FP}"
    source "${UTILS}"
  else
    echo -e "${ERROR_PREFIX} cannot read utilities file='${UTILS}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi
#  echo -e "${MESSAGE_PREFIX} moved this code to utilities file='${UTILS}'" 2>&1 | tee -a "${LOG_FP}"

  for CMD in \
    'setup_CMAQ_common_vars' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
# this fails: vars don't get set
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 7
    fi
  done

} # function setup_vars

### Look ahead for things that will abend the builders and runners in 'function run'
function check_space {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BLDMAKE_EXEC_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

#  # Default: don't remake `bldmake`: uber_driver::repo will cover this case. TODO: control via switch/envvar!
#  if [[ -x "${BLDMAKE_EXEC_FP}" ]] ; then
#    echo -e "${ERROR_PREFIX} bldmake executable='${BLDMAKE_EXEC_FP}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
#    exit 10
#  fi

  if [[ -z "${BCON_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} BCON_BUILD_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  # TODO: switch to overwrite
  if [[ -d "${BCON_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} BCON build dir='${BCON_BUILD_DIR}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  if [[ -z "${BCON_RUN_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} BCON_RUN_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  # TODO: switch to overwrite
  if [[ -d "${BCON_RUN_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} BCON run dir='${BCON_RUN_DIR}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  if [[ -z "${ICON_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON_BUILD_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  # TODO: switch to overwrite
  if [[ -d "${ICON_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON build dir='${ICON_BUILD_DIR}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  if [[ -z "${ICON_RUN_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON_RUN_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  # TODO: switch to overwrite
  if [[ -d "${ICON_RUN_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON run dir='${ICON_RUN_DIR}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  if [[ -z "${CCTM_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_BUILD_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  # TODO: switch to overwrite
  if [[ -d "${CCTM_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM build dir='${CCTM_BUILD_DIR}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

# TODO: 
#   if [[ -z "${CCTM_RUN_DIR}" ]] ; then
#     echo -e "${ERROR_PREFIX} CCTM_RUN_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
#     exit 10
#   fi

#   # TODO: switch to overwrite
#   if [[ -d "${CCTM_RUN_DIR}" ]] ; then
#     echo -e "${ERROR_PREFIX} CCTM run dir='${CCTM_RUN_DIR}' exists (move or delete, then rerun ${THIS_FN}), exiting ..." 2>&1 | tee -a "${LOG_FP}"
#     exit 10
#   fi

} # function check_space

function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'setup_vars' \
    'check_space' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
# this fails: vars (e.g., M3HOME) don't get exported (e.g., to function=inspect_all_output below)
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 10
    fi
  done
} # function setup

### Proceed to build and benchmark CMAQ.
function run {
  #  "${CLONER_EXEC_FP}" # move to uber_driver. CMAQ-build_driver.sh should build what we got.


  # Default: don't remake `bldmake`: uber_driver::repo will cover this case. TODO: control via switch/envvar!
  if   [[ -z "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BLDMAKE_EXEC_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 11
  elif [[ ! -x "${BLDMAKE_EXEC_FP}" ]] ; then
    # TODO: package this loop body for reuse below!
    for CMD in \
      "${BUILD_BLDMAKE_EXEC_FP}" \
    ; do
      if   [[ -z "${CMD}" ]] ; then
        echo -e "${ERROR_PREFIX} CMD not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
        exit 11
      elif [[ ! -x "${CMD}" ]] ; then
        echo -e "${ERROR_PREFIX} executable='${CMD}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
        exit 12
      fi

      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"

      if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
        echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
        exit 13
      fi
      # Don't fail if called executable fails: it may just be finding a previously-built object.
      # TODO: find/use protocol (except return codes, which `eval` hoses) discriminating between "failure"==found previously-built object and "real" failure.
    done # for CMD
  fi

  for CMD in \
    "${BUILD_BCON_EXEC_FP}" \
    "${RUN_BCON_EXEC_FP}" \
    "${BUILD_ICON_EXEC_FP}" \
    "${RUN_ICON_EXEC_FP}" \
    "${BUILD_CCTM_EXEC_FP}" \
  ; do
    if   [[ -z "${CMD}" ]] ; then
      echo -e "${ERROR_PREFIX} CMD not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 11
    elif [[ ! -x "${CMD}" ]] ; then
      echo -e "${ERROR_PREFIX} executable='${CMD}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 12
    fi

    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"

    if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
      echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
      exit 13
    fi
    # Don't fail if called executable fails: it may just be finding a previously-built object.
    # TODO: find/use protocol (except return codes, which `eval` hoses) discriminating between "failure"==found previously-built object and "real" failure.
  done # for CMD
} # function run

### What have we done?

function inspect {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'inspect_all_output' \
    'inspect_profiles' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 10
    fi
  done
} # function inspect

function inspect_all_output {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # TODO: make this actually exit!
  if   [[ -z "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} M3HOME not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 11
  elif [[ ! -d "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 12
  else

    ## Note unfortunately-necessary escape of `find` command!
    for CMD in \
      "ls -alt \$(find ${M3HOME}/ -type f -perm '-u+x' -name 'bldmake*')" \
      "ls -alt \$(find ${M3HOME}/ -type f -perm '-u+x' -name 'BCON*')" \
      "ls -alt \$(find ${M3HOME}/ -type f -perm '-u+x' -name 'ICON*')" \
      "ls -alt \$(find ${M3HOME}/ -type f -perm '-u+x' -name 'CCTM*')" \
    ; do
      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
        exit 13
      else
        echo 2>&1 | tee -a "${LOG_FP}" # newline
      fi
    done

  fi # if   [[ -z "${M3HOME}" ]]

} # function inspect_all_output

function inspect_profiles {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'inspect_BCON_profiles' \
    'inspect_ICON_profiles' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 10
    fi
  done

} # function inspect_profiles

# Note inspect_BCON_profiles == (inspect_ICON_profiles ~= s/icon/bcon/g). TODO: refactor
function inspect_BCON_profiles {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # These tests of M3HOME should be unnecessary here, but tests in function=inspect_all_output *don't* exit as intended
  if   [[ -z "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} M3HOME not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 11
  elif [[ ! -d "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 12
  else

    ## Note unfortunately-necessary escape of `find` command!
    for CMD in \
      "ls -alt \$(find ${M3HOME}/ -type f -name '${BCON_OUT_FN}')" \
    ; do
      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
        exit 16
      else
        echo 2>&1 | tee -a "${LOG_FP}" # newline
      fi
    done

    if   [[ -z "${BCON_OUT_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} BCON_OUT_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ ! -r "${BCON_OUT_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} no BCON-run output='${BCON_OUT_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${BCON_OUT_HEAD}" ]] ; then
      echo -e "${ERROR_PREFIX} BCON_OUT_HEAD not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${BCON_OUT_TAIL}" ]] ; then
      echo -e "${ERROR_PREFIX} BCON_OUT_TAIL not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${BCON_GOLD_HEAD}" ]] ; then
      echo -e "${ERROR_PREFIX} BCON_GOLD_HEAD not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${BCON_GOLD_TAIL}" ]] ; then
      echo -e "${ERROR_PREFIX} BCON_GOLD_TAIL not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ ! -r "${BCON_GOLD_HEAD}" ]] ; then
      echo -e "${ERROR_PREFIX} no golden BCON-run output='${BCON_GOLD_HEAD}'. TODO: copy it!" 2>&1 | tee -a "${LOG_FP}"
    elif [[ ! -r "${BCON_GOLD_TAIL}" ]] ; then
      echo -e "${ERROR_PREFIX} no golden BCON-run output='${BCON_GOLD_TAIL}'. TODO: copy it!" 2>&1 | tee -a "${LOG_FP}"
    else
      for CMD in \
        "${NCDUMP} -h ${BCON_OUT_FP} | head -n $(wc -l < ${BCON_GOLD_HEAD}) > ${BCON_OUT_HEAD}" \
        "${NCDUMP} -h ${BCON_OUT_FP} | tail -n $(wc -l < ${BCON_GOLD_TAIL}) > ${BCON_OUT_TAIL}" \
        "diff -wB ${BCON_GOLD_HEAD} ${BCON_OUT_HEAD}" \
        "diff -wB ${BCON_GOLD_TAIL} ${BCON_OUT_TAIL}" \
      ; do
        echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
        eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
          exit 17
        else
          echo 2>&1 | tee -a "${LOG_FP}" # newline
        fi
      done
    fi # if   [[ ! -r "${BCON_GOLD_HEAD}" ]]
  fi # if   [[ -z "${M3HOME}" ]]
} # function inspect_BCON_profiles

# Note inspect_ICON_profiles == (inspect_BCON_profiles ~= s/bcon/icon/g). TODO: refactor
function inspect_ICON_profiles {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # These tests of M3HOME should be unnecessary here, but tests in function=inspect_all_output *don't* exit as intended
  if   [[ -z "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} M3HOME not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 11
  elif [[ ! -d "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 12
  else

    ## Note unfortunately-necessary escape of `find` command!
    for CMD in \
      "ls -alt \$(find ${M3HOME}/ -type f -name '${ICON_OUT_FN}')" \
    ; do
      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
        exit 16
      else
        echo 2>&1 | tee -a "${LOG_FP}" # newline
      fi
    done

    if   [[ -z "${ICON_OUT_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} ICON_OUT_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ ! -r "${ICON_OUT_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} no ICON-run output='${ICON_OUT_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${ICON_OUT_HEAD}" ]] ; then
      echo -e "${ERROR_PREFIX} ICON_OUT_HEAD not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${ICON_OUT_TAIL}" ]] ; then
      echo -e "${ERROR_PREFIX} ICON_OUT_TAIL not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${ICON_GOLD_HEAD}" ]] ; then
      echo -e "${ERROR_PREFIX} ICON_GOLD_HEAD not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ -z "${ICON_GOLD_TAIL}" ]] ; then
      echo -e "${ERROR_PREFIX} ICON_GOLD_TAIL not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 17
    elif [[ ! -r "${ICON_GOLD_HEAD}" ]] ; then
      echo -e "${ERROR_PREFIX} no golden ICON-run output='${ICON_GOLD_HEAD}'. TODO: copy it!" 2>&1 | tee -a "${LOG_FP}"
    elif [[ ! -r "${ICON_GOLD_TAIL}" ]] ; then
      echo -e "${ERROR_PREFIX} no golden ICON-run output='${ICON_GOLD_TAIL}'. TODO: copy it!" 2>&1 | tee -a "${LOG_FP}"
    else
      for CMD in \
        "${NCDUMP} -h ${ICON_OUT_FP} | head -n $(wc -l < ${ICON_GOLD_HEAD}) > ${ICON_OUT_HEAD}" \
        "${NCDUMP} -h ${ICON_OUT_FP} | tail -n $(wc -l < ${ICON_GOLD_TAIL}) > ${ICON_OUT_TAIL}" \
        "diff -wB ${ICON_GOLD_HEAD} ${ICON_OUT_HEAD}" \
        "diff -wB ${ICON_GOLD_TAIL} ${ICON_OUT_TAIL}" \
      ; do
        echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
        eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${ERROR_PREFIX} '${CMD}' failed, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
          exit 17
        else
          echo 2>&1 | tee -a "${LOG_FP}" # newline
        fi
      done
    fi # if   [[ ! -r "${ICON_GOLD_HEAD}" ]]
  fi # if   [[ -z "${M3HOME}" ]]
} # function inspect_ICON_profiles

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main loop:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#  'setup' \ # sources, resources
#  'run' \ # and build
#  'inspect' \ # what have we done?
#  'teardown' # as needed
for CMD in \
  'setup' \
  'run' \
  'inspect' \
; do
  echo -e "\n$ ${MESSAGE_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${LOG_FP}"
# this fails: vars (e.g., M3HOME) don't get exported (e.g., to function=inspect_all_output)
#  eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
  eval "${CMD}"
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
    exit 15
  fi
done

exit 0 # victory!

*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

This is a `git` repository for [CMAQ grid definition files][GRIDDESC], built using [CMAQ-build][CMAQ-build/one_way_tarsplat_to_git.sh @ bitbucket]. Note that currently CMAQ (as of [version=5.0.1][CMAQ-5.0.1 @ CMAS wiki]) distributes the `GRIDDESC` file for the [CMAQ benchmark][Benchmarking CMAQ] in folder=`scripts` of the [CMAQ tarballs][CMAQ-5.0.1 download page]; we seek to manage these important files somewhat better. 

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/tlroche/aqmeii-na_n2o/wiki/Home
[GRIDDESC]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#GRIDDESC:_Horizontal_domain_definition
[CMAQ-build/one_way_tarsplat_to_git.sh @ bitbucket]: https://bitbucket.org/tlroche/cmaq-build/src/HEAD/one_way_tarsplat_to_git.sh?at=master
[CMAQ-5.0.1 @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD
[Benchmarking CMAQ]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Benchmarking
[CMAQ-5.0.1 download page]: https://www.cmascenter.org/download/software/cmaq/cmaq_5-0-1.cfm

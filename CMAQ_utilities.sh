### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Code common to the über-project=CMAQ, particularly to CMAQ-build_driver.sh and uber_driver.sh.
### For details see https://bitbucket.org/epa_n2o_project_team/cmaq-build/src/HEAD/README.md

### This code (err ... this whole file!) draws heavily on the pair {BCON-build/BCON_utilities.sh, ICON-build/ICON_utilities.sh}. TODO: refactor.
function setup_CMAQ_common_vars {
  local MESSAGE_PREFIX='CMAQ_utilities.sh:'
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  # Take whatever LOG_FP is in the environment?

  ### I expect all project repos to be peer dirs: see CMAQ-build/repo_creator.sh
  CMAQ_BUILD_DIR="$(dirname ${THIS_DIR})/CMAQ-build"
  # For discussion of config.cmaq, see
  # http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
  # but we depend on the bash version!
  CONFIG_CMAQ_FN='config.cmaq.sh'
  export CONFIG_CMAQ_PATH="${THIS_DIR}/${CONFIG_CMAQ_FN}"

  ### Most important environment variables (envvars) for config.cmaq:
  ### see CMAQ-build/uber.config.cmaq.sh for details
  UBER_CONFIG_CMAQ_FN='uber.config.cmaq.sh'
  export UBER_CONFIG_CMAQ_PATH="${THIS_DIR}/${UBER_CONFIG_CMAQ_FN}"

  ### M3DATA not needed for build, and neither COMPILER nor INFINITY needed for run.
  ### However, one really should be running after build, if only to test.
  # TODO: test COMPILER as enum.
  # TODO: nuke the INFINITY kludge, or test as binary.
  if [[ -z "${COMPILER}" || -z "${INFINITY}" || -z "${M3DATA}" || -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" ]] ; then
    if [[ -r "${UBER_CONFIG_CMAQ_PATH}" ]] ; then
      echo -e "${MESSAGE_PREFIX} about to 'source ${UBER_CONFIG_CMAQ_PATH}'" 2>&1 | tee -a "${LOG_FP}"
      source "${UBER_CONFIG_CMAQ_PATH}" # cannot log/tee?
    else
      echo -e "${ERROR_PREFIX} cannot find uber.config.cmaq, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    fi
  fi

  if [[ -z "${COMPILER}" || -z "${INFINITY}" || -z "${M3DATA}" || -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" ]] ; then
    # still?
    echo -e "${ERROR_PREFIX} one of COMPILER, INFINITY, M3DATA, M3LIB, M3HOME, M3MODEL is not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  else

    if [[ ! -r "${CONFIG_CMAQ_PATH}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot find '${CONFIG_CMAQ_PATH}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    else
      echo -e "${MESSAGE_PREFIX} about to 'source ${CONFIG_CMAQ_PATH}'" 2>&1 | tee -a "${LOG_FP}"
      source "${CONFIG_CMAQ_PATH}" # cannot log/tee?
    fi # if [[ ! -r "${CONFIG_CMAQ_PATH}" ]]
  fi # [[ -z "${M3LIB}" ||

  if   [[ -z "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} M3HOME not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  else
    echo -e "${MESSAGE_PREFIX} M3HOME='${M3HOME}'" 2>&1 | tee -a "${LOG_FP}"
  fi

  if   [[ -z "${M3LIB}" ]] ; then
    echo -e "${ERROR_PREFIX} M3LIB not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 3
  else
    echo -e "${MESSAGE_PREFIX} M3LIB='${M3LIB}'" 2>&1 | tee -a "${LOG_FP}"
  fi

  if   [[ -z "${M3MODEL}" ]] ; then
    echo -e "${ERROR_PREFIX} M3MODEL not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 4
  else
    echo -e "${MESSAGE_PREFIX} M3MODEL='${M3MODEL}'" 2>&1 | tee -a "${LOG_FP}"
  fi

  if   [[ -z "${COMPILER}" ]] ; then
    echo -e "${ERROR_PREFIX} COMPILER not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  else
    echo -e "${MESSAGE_PREFIX} COMPILER='${COMPILER}'" 2>&1 | tee -a "${LOG_FP}"
  fi

  # should not usually be necessary
  if   [[ -z "${INFINITY}" ]] ; then
    echo -e "${ERROR_PREFIX} INFINITY not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 6
  else
    echo -e "${MESSAGE_PREFIX} INFINITY='${INFINITY}'" 2>&1 | tee -a "${LOG_FP}"
  fi

  if [[ ! -d "${M3LIB}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3LIB='${M3LIB}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 8
  elif [[ ! -d "${M3DATA}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3DATA='${M3DATA}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 9
  # to be created!
#   elif   [[ ! -d "${M3HOME}" ]] ; then
#     echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
#     exit 7
#   elif [[ ! -d "${M3MODEL}" ]] ; then
#     echo -e "${ERROR_PREFIX} cannot read M3MODEL='${M3MODEL}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
#     exit 10
  fi
  echo 2>&1 | tee -a "${LOG_FP}" # newline

  ### just a synonym for M3DATA
  INPUT_ROOT="${M3DATA}"

  ### git-related
  # SUPER_TAG='5.0.1_release_built_infinity_20130918' # desired `git` tag for repos
  # We expect all repositories to be peer folders, i.e., all directories directly under this one:
  REPO_ROOT="$(readlink -f ${THIS_DIR}/..)"

  ## helpers and spaces used below
  # CLONER_FP="${THIS_DIR}/clone_repos.sh" # move to uber_driver. CMAQ-build_driver.sh should build what we got.

  BCON_BUILD_REPO_DIR="${REPO_ROOT}/BCON-build"              # repo of build/run code
  export BUILD_BCON_EXEC_FP="${BCON_BUILD_REPO_DIR}/build_BCON.sh"  # builder code
  export BCON_BUILD_DIR="${M3HOME}/BLD_BCON_${UBER_APPL}"           # build space
  export RUN_BCON_EXEC_FP="${BCON_BUILD_REPO_DIR}/run_BCON.sh"      # runner code
  export BCON_RUN_DIR="${M3HOME}/RUN_BCON_${UBER_APPL}"             # run space

  BLDMAKE_REPO_DIR="${REPO_ROOT}/BLDMAKE"
  export BLDMAKE_BUILD_REPO_DIR="${REPO_ROOT}/BLDMAKE-build"
  export BUILD_BLDMAKE_EXEC_FP="${BLDMAKE_BUILD_REPO_DIR}/build_BLDMAKE.sh"
  export BLDMAKE_BUILD_DIR="${BLDMAKE_BUILD_REPO_DIR}"
  # BLDMAKE_RUN_DIR has no semantics in this usecase, or rather multiple
  export BLDMAKE_EXEC_FP="${BLDMAKE_REPO_DIR}/bldmake"

  ### build dir--not THIS_DIR, but the space in which we build
  # how to discover the target executable? more below, following were recovered *after* build
  CMAQ_EXEC_FN="${CMAQ_EXEC_PREFIX}_${EXEC_ID}"
  export MODEL="${CMAQ_EXEC_FN}" # Makefile wants this name
  CMAQ_BUILD_DIR_NAME="BLD_${CMAQ_EXEC_PREFIX}"
  export CMAQ_BUILD_DIR_PATH="${M3HOME}/${CMAQ_BUILD_DIR_NAME}"
  export CMAQ_EXEC_FP="${CMAQ_BUILD_DIR_PATH}/${CMAQ_EXEC_FN}"

  CCTM_BUILD_REPO_DIR="${REPO_ROOT}/CCTM-build"
  export BUILD_CCTM_EXEC_FP="${CCTM_BUILD_REPO_DIR}/build_CCTM.sh"
  export CCTM_BUILD_DIR="${M3HOME}/BLD_CCTM_${UBER_APPL}"
  # TODO: 
  # RUN_CCTM_EXEC_FP="${CCTM_BUILD_REPO_DIR}/run_CCTM.sh"
  # CCTM_RUN_DIR="${M3HOME}/RUN_CCTM_${UBER_APPL}"

  ICON_BUILD_REPO_DIR="${REPO_ROOT}/ICON-build"
  export BUILD_ICON_EXEC_FP="${ICON_BUILD_REPO_DIR}/build_ICON.sh"
  export ICON_BUILD_DIR="${M3HOME}/BLD_ICON_${UBER_APPL}"
  export RUN_ICON_EXEC_FP="${ICON_BUILD_REPO_DIR}/run_ICON.sh"
  export ICON_RUN_DIR="${M3HOME}/RUN_ICON_${UBER_APPL}"

  ### Resources for testing
  ## These "tags" widely used by our builds and runs. TODO: move up to *config.cmaq*
  UBER_APPL='CMAQv5_0_1'
  GOLD_APPL='V5g'
  UBER_CFG='CMAQ-BENCHMARK'
  GOLD_CFG="${UBER_CFG}"
  # unfortunately gotta parameterize NCDUMP, since it's an alias on HPCC :-( which can be a drag sometimes.
  export NCDUMP='/share/linux86_64/grads/supplibs-2.2.0/x86_64-unknown-linux-gnu/bin/ncdump'

  ## BCON output and golden oracles
  BCON_OUT_DIR_NAME="RUN_BCON_${UBER_APPL}"
  BCON_GOLD_DIR_NAME="${BCON_OUT_DIR_NAME}.0"
  export BCON_GOLD_DIR="${M3HOME}/${BCON_GOLD_DIR_NAME}"
  BCON_OUT_FN="BCON_${UBER_APPL}_${UBER_CFG}_profile"
  BCON_GOLD_FN="BCON_${GOLD_APPL}_${GOLD_CFG}_profile"
  export BCON_OUT_DIR="${M3HOME}/${BCON_OUT_DIR_NAME}"
  export BCON_GOLD_FP="${BCON_GOLD_DIR}/${BCON_GOLD_FN}"
  export BCON_OUT_FP="${BCON_OUT_DIR}/${BCON_OUT_FN}"
  export BCON_OUT_HEAD="${BCON_OUT_FP}.head"
  export BCON_GOLD_HEAD="${BCON_GOLD_FP}.head"
  export BCON_OUT_TAIL="${BCON_OUT_FP}.tail"
  export BCON_GOLD_TAIL="${BCON_GOLD_FP}.tail"

  ## ICON output and golden oracles
  ICON_OUT_DIR_NAME="RUN_ICON_${UBER_APPL}"
  ICON_GOLD_DIR_NAME="${ICON_OUT_DIR_NAME}.0"
  export ICON_GOLD_DIR="${M3HOME}/${ICON_GOLD_DIR_NAME}"
  ICON_OUT_FN="ICON_${UBER_APPL}_${UBER_CFG}_profile"
  ICON_GOLD_FN="ICON_${GOLD_APPL}_${GOLD_CFG}_profile"
  export ICON_OUT_DIR="${M3HOME}/${ICON_OUT_DIR_NAME}"
  export ICON_GOLD_FP="${ICON_GOLD_DIR}/${ICON_GOLD_FN}"
  export ICON_OUT_FP="${ICON_OUT_DIR}/${ICON_OUT_FN}"
  export ICON_OUT_HEAD="${ICON_OUT_FP}.head"
  export ICON_GOLD_HEAD="${ICON_GOLD_FP}.head"
  export ICON_OUT_TAIL="${ICON_OUT_FP}.tail"
  export ICON_GOLD_TAIL="${ICON_GOLD_FP}.tail"

} # function setup_CMAQ_common_vars

*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

This is a `git` repository for `includes` code used to build [CCTM][CCTM overview @ CMAS wiki], constructed from the `cvs`-based [tarballs][CMAQ-5.0.1 download page] distributed for [CMAQ version=5.0.1][CMAQ-5.0.1 @ CMAS wiki]. It was built using [CMAQ-build][CMAQ-build/one_way_tarsplat_to_git.sh @ bitbucket].

[CCTM overview @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#CMAQ_Chemistry-Transport_Model_.28CCTM.29
[AQMEII-NA_N2O wiki home]: https://bitbucket.org/tlroche/aqmeii-na_n2o/wiki/Home
[CMAQ-5.0.1 download page]: https://www.cmascenter.org/download/software/cmaq/cmaq_5-0-1.cfm
[CMAQ-5.0.1 @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD
[CMAQ-build/one_way_tarsplat_to_git.sh @ bitbucket]: https://bitbucket.org/tlroche/cmaq-build/src/HEAD/one_way_tarsplat_to_git.sh?at=master

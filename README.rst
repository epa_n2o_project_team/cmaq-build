*(part of the* `AQMEII-NA_N2O <https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home>`_ *family of projects)*

.. contents:: **Table of Contents**

open-source notice
==================

Copyright 2013, 2014 ``Tom Roche <Tom_Roche@pobox.com>``

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the `GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html>`_ as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the `GNU Affero General Public License <./COPYING>`_ for more details.

.. figure:: ../../downloads/Affero_badge__agplv3-155x51.png
   :scale: 100 %
   :alt: distributed under the GNU Affero General Public License
   :align: left

overview
========

This is the top-level project/repository for building and running instances of `CMAQ version=5.0.1 <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD>`_. Using various of its tools, one can (numbering for reference only)

1. Download and unpack the currently-distributed `tarballs <https://www.cmascenter.org/download/software/cmaq/cmaq_5-0-1.cfm>`_ for CMAQ-5.0.1.
#. `Create git repositories <../../src/HEAD/one_way_tarsplat_to_git.sh>`_ for the currently CVS-based code if needed to recreate any of the subproject repos below.
#. Provide/utilize a single store for important "top-level" build and run variables common to all subproject tools.
#. *(in progress)* Build and run CMAQ "end to end" for a given application.

CMAQ-build project family
-------------------------

Follow links for more information regarding

* component function (links to relevant section in `current CMAQ Operational Guidance Document (OGD) <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD>`_)
* repository structure/function (links to repo homepage/README)

(Hopefully my repositories will disappear as CMAS (or other entity) hosts *official* git repos.)

.. Note the following must be a "grid table", since "simple table" can't cope with links (or, at least, the bitbucket/docutils-0.7 renderer can't.

+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| description of project or CMAQ component                                                                                                                        | repository name                                                |
+=================================================================================================================================================================+================================================================+
| `BCON <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Boundary_Conditions_Processor_.28BCON.29>`_  | `BCON <https://bitbucket.org/epa_n2o_project_team/bcon>`_                   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Code for building my `BCON <https://bitbucket.org/epa_n2o_project_team/bcon>`_ repo                                                                                          | `BCON-build <https://bitbucket.org/epa_n2o_project_team/bcon-build>`_       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `BLDMAKE <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Model_Builder_.28Bldmake.29>`_                | `BLDMAKE <https://bitbucket.org/epa_n2o_project_team/bldmake>`_             |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Code for building my `BLDMAKE <https://bitbucket.org/epa_n2o_project_team/bldmake>`_ repo                                                                                    | `BLDMAKE-build <https://bitbucket.org/epa_n2o_project_team/bldmake-build>`_ |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `CCTM <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#CMAQ_Chemistry-Transport_Model_.28CCTM.29>`_ | `CCTM <https://bitbucket.org/epa_n2o_project_team/cctm>`_                   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Code for building my `CCTM <https://bitbucket.org/epa_n2o_project_team/cctm>`_ repo                                                                                          | `CCTM-build <https://bitbucket.org/epa_n2o_project_team/cctm-build>`_       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| *(this project)*                                                                                                                                                | `CMAQ-build <https://bitbucket.org/epa_n2o_project_team/cmaq-build>`_       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| project to manage CMAQ grid definition files (aka `GRIDDESC <TODO>`_) which are included in tarballs with miscellaneous scripts                                 | `CMAQ-grids <https://bitbucket.org/epa_n2o_project_team/cmaq-grids>`_       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| ``includes`` code for CCTM                                                                                                                                      | `ICL <https://bitbucket.org/epa_n2o_project_team/icl>`_                     |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Code for building my `ICL <https://bitbucket.org/epa_n2o_project_team/icl>`_ repo                                                                                            | `ICL-build <ICL-build @ bitbucket>`_                           |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `ICON <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Initial_Conditions_Processor_.28ICON.29>`_       | `ICON <https://bitbucket.org/epa_n2o_project_team/icon>`_                   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Code for building my `ICON <https://bitbucket.org/epa_n2o_project_team/icon>`_ repo                                                                                          | `ICON-build <https://bitbucket.org/epa_n2o_project_team/icon-build>`_       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `JPROC <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Photolysis_Rate_Processor_.28JPROC.29>`_        | `JPROC <https://bitbucket.org/epa_n2o_project_team/jproc>`_                 |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| chemical-mechanism code for CCTM																  | `MECHS <https://bitbucket.org/epa_n2o_project_team/mechs>`_                 |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| Code for building my `MECHS <https://bitbucket.org/epa_n2o_project_team/mechs>`_ repo                                                                                        | `MECHS-build <MECHS-build @ bitbucket>`_                       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `PARIO <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#PARIO>`_                                        | `PARIO <https://bitbucket.org/epa_n2o_project_team/pario>`_                 |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `PROCAN <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#PROCAN>`_                                      | `PROCAN <https://bitbucket.org/epa_n2o_project_team/procan>`_               |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| `STENEX <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#STENEX>`_                                      | `STENEX <https://bitbucket.org/epa_n2o_project_team/stenex>`_               |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| miscellaneous tools distributed with CMAQ															  | `TOOLS <https://bitbucket.org/epa_n2o_project_team/tools>`_                 |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+

**Note**: there are no ``-build`` repos for the following source repos, which I haven't yet needed to build:

* ``CMAQ-grids``: these are data-like objects that should not require processing (other than by the operator, who must ensure their grids are supported).
* ``JPROC``: I used a photolysis table (at least for the benchmark).
* ``PARIO``, ``PROCAN``, ``STENEX``: I used the versions of these libraries previously built on infinity.
* ``TOOLS``: I haven't needed any of these.

That being said, providing build code for these should be trivial, if needed.

TODOs
=====

1. Move all these TODOs to `issue tracker <../../issues>`_.
#. Implement ``run_CCTM.sh`` for CMAQ benchmark! Cease to use {``bldmake``, `bldit.cctm <https://bitbucket.org/epa_n2o_project_team/cctm-build/src/HEAD/bldit.cctm>`_}, convert to all-``bash`` build.
#. for all subproject ``{build_*, run_*}.sh``: move definition/call of ``*_utilities`` inside function ``setup_vars``.
#. `CMAQ-build_driver.sh <../../src/HEAD/CMAQ-build_driver.sh>`_\-related:

    #. find/use protocol (except return codes, which ``eval`` hoses) discriminating between "failure"==found previously-built object and "real" failure. Use that to provide mechanism to switch behavior on finding previously-built objects: die or ignore.
    #. better manage the "golden" test oracles for run outputs

#. `repo_creator.sh <../../src/HEAD/repo_creator.sh>`_: handle partial outputs (e.g., some but not all tarballs cached)
#. `build_CCTM.sh <https://bitbucket.org/epa_n2o_project_team/cctm-build/src/HEAD/build_CCTM.sh>`_: separately test the major env vars (i.e., copy/mod from ``CMAQ-build_driver.sh``)
#. `clone_repos.sh <../../src/HEAD/clone_repos.sh>`_:

    #. absorb ``CVS_TAG`` from `one_way_tarsplat_to_git.sh <../../src/HEAD/one_way_tarsplat_to_git.sh>`_
    #. run to test (i.e., check for ``one_way_tarsplat_to_git.sh`` breakage)

#. new helper (to be created, if needed): to build `JPROC <TODO>`_ using repo `JPROC-build <https://bitbucket.org/epa_n2o_project_team/jproc>`_

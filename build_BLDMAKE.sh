#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Build `bldmake` executable in repo=BLDMAKE using repo=BLDMAKE-build.
### (Lots of redundancy there :-)
### Build superproject=BLDMAKE-build and its "submodule" repo=BLDMAKE.
### (TODO: make BLDMAKE an actual submodule.)
### For details see https://bitbucket.org/epa_n2o_project_team/bldmake-build/raw/HEAD/README.md
### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `cp`
### * `dirname`
### * `ls`
### * `readlink`
### * `tail`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_NAME="$(basename ${THIS})"

### logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_NAME="${THIS_NAME}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_PATH="${THIS_DIR}/${LOG_NAME}"

### Most important environment variables (envvars) for config.cmaq:
### see CMAQ-build/uber.config.cmaq.sh for details
export UBER_CONFIG_CMAQ_PATH="${THIS_DIR}/uber.config.cmaq.sh"
if   [[ -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" || -z "${COMPILER}" || -z "${INFINITY}" ]] ; then
#  source "${UBER_CONFIG_CMAQ_PATH}" 2>&1 | tee -a "${LOG_PATH}"
# cannot pipe and `source`?
  echo -e "$ source ${UBER_CONFIG_CMAQ_PATH}" 2>&1 | tee -a "${LOG_PATH}"
  source "${UBER_CONFIG_CMAQ_PATH}"
fi

if   [[ -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" || -z "${COMPILER}" || -z "${INFINITY}" ]] ; then
  echo -e "${ERROR_PREFIX} one of COMPILER, INFINITY, M3LIB, M3HOME, M3MODEL is not defined, exiting ..."
  exit 2
elif [[ ! -d "${M3HOME}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..."
  exit 3
elif [[ ! -d "${M3LIB}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3LIB='${M3LIB}', exiting ..."
  exit 4
# to be created!
# elif [[ ! -z "${M3MODEL}" ]] ; then
#   echo -e "${ERROR_PREFIX} cannot read M3MODEL='${M3MODEL}', exiting ..."
#   exit 5
fi

### For discussion of config.cmaq, see
### http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
export CONFIG_CMAQ_PATH="${THIS_DIR}/config.cmaq"
export CSH_EXEC='/bin/tcsh' # since config.cmaq is (currently) a csh script.

### BLDMAKE: the repo with the sources we're building
### BLDMAKE-build: the repo for build code for BLDMAKE
export BLDMAKE_DIR="${M3HOME}/BLDMAKE"
export BLDMAKE_EXEC="${BLDMAKE_DIR}/bldmake" # the target executable
BLDMAKE_BUILD_DIR="${M3HOME}/BLDMAKE-build"
BLDMAKE_BUILD_EXEC="${BLDMAKE_BUILD_DIR}/bldit.bldmake"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Don't build if ...
### ... we have nothing from which to build.
if [[ ! -d "${BLDMAKE_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find repo=BLDMAKE @ '${BLDMAKE_DIR}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 6
fi

### ... we have nothing with which to build.
if [[ ! -d "${BLDMAKE_BUILD_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find repo=BLDMAKE-build @ '${BLDMAKE_BUILD_DIR}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 7
fi
if [[ ! -x "${BLDMAKE_BUILD_EXEC}" ]] ; then
  echo -e "${ERROR_PREFIX} BLDMAKE_BUILD_EXEC='${BLDMAKE_BUILD_EXEC}' not executable. Exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 8
fi

### ... we already have the executable.
if [[ -x "${BLDMAKE_EXEC}" ]] ; then
  ls -al "${BLDMAKE_EXEC}"
  echo -e "${THIS_NAME}: BLDMAKE_EXEC='${BLDMAKE_EXEC}' exists: move or delete." 2>&1 | tee -a "${LOG_PATH}"
  exit 9
fi

### Can't build if we don't have csh :-( TODO: rewrite CMAQ build scripts in bash!

if [[ ! -x "${CSH_EXEC}" ]] ; then
  echo -e "${ERROR_PREFIX} CSH_EXEC='${CSH_EXEC}' not executable. Exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 10
fi

### Use our config.cmaq

if [[ -r "${CONFIG_CMAQ_PATH}" ]] ; then
  ## Copy our config.cmaq to BLDMAKE ...
  cp "${CONFIG_CMAQ_PATH}" "${BLDMAKE_BUILD_DIR}"
  ## ... then tell BLDMAKE where to find config.cmaq
  export CONFIG_CMAQ_PATH="${BLDMAKE_BUILD_DIR}/config.cmaq"
else
  echo -e "${ERROR_PREFIX} cannot read our config.cmaq @ '${CONFIG_CMAQ_PATH}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 11
fi

# Time to build.
# TODO: add logging!
for CMD in \
  "ls -alh ${BLDMAKE_DIR}/" \
  "${BLDMAKE_BUILD_EXEC}" \
; do
  echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_PATH}"
  eval "${CMD}" 2>&1 | tee -a "${LOG_PATH}"
  if [[ -n "$(tail -n 3 ${LOG_PATH} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
    echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_PATH}"
    exit 12
  fi
done

if [[ ! -x "${BLDMAKE_EXEC}" ]] ; then
  echo -e "${THIS_NAME}: failed to create '${BLDMAKE_EXEC}'!"
  ls -alh "${BLDMAKE_DIR}"
  exit 13
fi

exit 0 # success!

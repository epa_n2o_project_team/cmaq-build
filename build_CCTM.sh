#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Build project=CCTM.
### For details see https://bitbucket.org/epa_n2o_project_team/cctm-build/raw/HEAD/README.md
### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `date`
### * `dirname`
### * `find`
### * `head`
### * `ls`
### * `pwd`
### * `readlink`
### * `tail`
### * `tee`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:

THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_NAME="$(basename ${THIS})"
ERROR_PREFIX="${THIS_NAME}: ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_NAME="${THIS_NAME}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_PATH="${THIS_DIR}/${LOG_NAME}"
# SUPER_NAME='CCTM-build' # name of the superproject

## Most important environment variables (envvars) for config.cmaq:
## see CMAQ-build/uber.config.cmaq.sh for details
export UBER_CONFIG_CMAQ_PATH="${THIS_DIR}/uber.config.cmaq.sh"
if [[ -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" || -z "${COMPILER}" || -z "${INFINITY}" ]] ; then
  source "${UBER_CONFIG_CMAQ_PATH}"
fi

# TODO: test COMPILER as enum.
# TODO: nuke the INFINITY kludge, or test as binary.
if   [[ -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" || -z "${COMPILER}" || -z "${INFINITY}" ]] ; then
  echo -e "${ERROR_PREFIX} one of COMPILER, INFINITY, M3LIB, M3HOME, M3MODEL is not defined, exiting ..."
  exit 2
elif [[ ! -d "${M3HOME}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..."
  exit 2
elif [[ ! -d "${M3LIB}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3LIB='${M3LIB}', exiting ..."
  exit 2
elif [[ ! -d "${M3MODEL}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3MODEL='${M3MODEL}', exiting ..."
  exit 2
fi

# For discussion of config.cmaq, see
# http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
export CONFIG_CMAQ_PATH="${THIS_DIR}/config.cmaq"
export CSH_EXEC='/bin/tcsh' # since config.cmaq is (currently) a csh script.
# SUPER_TAG='5.0.1_release_built_infinity_20130918' # desired `git` tag for superproject

# ## for building superproject
# M3HOME_BACKUP="${M3HOME}.bak"
# # bitbucket forces lower-case, and infinity does not have bash >= 4
# PROJECT_NAME="$(echo ${SUPER_NAME} | tr '[:upper:]' '[:lower:]')"
# REMOTE_REPO="bitbucket.org/tlroche/${PROJECT_NAME}.git"
# REMOTE_SSH_URI="ssh://git@${REMOTE_REPO}"
# # gotta use HTTP on EPA systems: can't SSH out. Worse ...
# # REMOTE_HTTP_URI="https://tlroche@${REMOTE_REPO}" # don't need user for cloning
# REMOTE_HTTP_URI="https://${REMOTE_REPO}"
# ... EPA systems don't have SSL certificates, so lotsa password typing :-(
GIT_REMOTE_PREFIX='env GIT_SSL_NO_VERIFY=true'
# off EPA this should not be a problem
# GIT_REMOTE_PREFIX=''

## prerequisites:

# bldmake
export BLDMAKE_DIR="${M3HOME}/BLDMAKE"
# export BLDMAKE_BUILDER="${BLDMAKE_DIR}/bldit.bldmake"
export BLDMAKE_EXEC="${BLDMAKE_DIR}/bldmake"

# repo=CCTM
export M3MODEL="${M3HOME}/CCTM" # repo with the CCTM sources
# how to discover the target executable? more below, following were recovered *after* build
CCTM_EXEC_STRING='CCTM_CMAQv5_0_1'
CCTM_EXEC_NAME="${CCTM_EXEC_STRING}_Linux2_x86_64intel"
CCTM_EXEC_BUILD_DIR_NAME="BLD_${CCTM_EXEC_STRING}"
CCTM_EXEC_DIR="${M3HOME}/${CCTM_EXEC_BUILD_DIR_NAME}"
CCTM_EXEC="${CCTM_EXEC_DIR}/${CCTM_EXEC_NAME}"

# repo=CCTM-build
CCTM_BUILD_DIR="${M3HOME}/CCTM-build"
CCTM_BUILD_EXEC="${CCTM_BUILD_DIR}/bldit.cctm"

# repo=ICL
export CCTM_INCLUDES_DIR="${M3HOME}/ICL"

# repo=MECHS
export CCTM_MECHS_DIR="${M3HOME}/MECHS"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Don't build if ...

## ... we already have the executable we seek to build.
## Unfortunately the name and even the dir of the target executable is determined in ${CCTM_BUILD_EXEC}, which is csh, and gets called later.
## TODO: move setting of path of target executable up!
if [[ -x "${CCTM_EXEC}" ]] ; then
  ls -al "${CCTM_EXEC}"
  echo -e "${ERROR_PREFIX} CCTM executable '${CCTM_EXEC}' exists: move or delete." 2>&1 | tee -a "${LOG_PATH}"
  exit 2
fi

## ... we have nothing from which to build.

# CCTM sources
if [[ ! -d "${M3MODEL}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find repo with CCTM sources @ '${M3MODEL}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 3
fi

# CCTM includes
if [[ ! -d "${CCTM_INCLUDES_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find repo with CCTM includes @ '${CCTM_INCLUDES_DIR}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 4
fi

# CCTM mechanisms
if [[ ! -d "${CCTM_MECHS_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find repo with CCTM mechanisms @ '${CCTM_MECHS_DIR}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 5
fi

## ... we have nothing with which to build.

if [[ ! -d "${CCTM_BUILD_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find repo=CCTM-build @ '${CCTM_BUILD_DIR}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 6
fi

if [[ ! -x "${CCTM_BUILD_EXEC}" ]] ; then
  echo -e "${ERROR_PREFIX} CCTM_BUILD_EXEC='${CCTM_BUILD_EXEC}' not executable. Exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 7
fi

# TODO: rewrite CMAQ build scripts in bash!
if [[ ! -x "${CSH_EXEC}" ]] ; then
  echo -e "${ERROR_PREFIX} CSH_EXEC='${CSH_EXEC}' not executable. Exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 8
fi

### Use our config.cmaq

if [[ -r "${CONFIG_CMAQ_PATH}" ]] ; then
  ## Copy our config.cmaq to CCTM ...
  cp "${CONFIG_CMAQ_PATH}" "${CCTM_BUILD_DIR}"
  ## ... then tell CCTM where to find config.cmaq
  export CONFIG_CMAQ_PATH="${CCTM_BUILD_DIR}/config.cmaq"
else
  echo -e "${ERROR_PREFIX} cannot read our config.cmaq @ '${CONFIG_CMAQ_PATH}', exiting ..." 2>&1 | tee -a "${LOG_PATH}"
  exit 9
fi

### Setup CCTM-build

# ## why does `pushd` fail inside `eval` loops, but ONLY WHEN PIPING OUTPUT ???
# echo -e "$ pushd ${CCTM_BUILD_DIR}" 2>&1 | tee -a "${LOG_PATH}"
# pushd "${CCTM_BUILD_DIR}"

## (TODO: consume tag:
##  * Checkout desired tag of CCTM-build
##  * Recreate branch=master (after checkout-ing tag), otherwise we're detached HEAD (due to tag)
##  1. Delete local branch=master: "${GIT_REMOTE_PREFIX} git checkout tags/${SUPER_TAG}"
##  2. Delete remote branch=master ?: "${GIT_REMOTE_PREFIX} git branch -D master"
##  3. Create local branch=master: "${GIT_REMOTE_PREFIX} git checkout -b master"
## )

### Use ${CCTM_BUILD_EXEC} to build source submodules=CCTM with include submodules={ICL, MECHS}

# What is
# "ls -alt $(ls -1dt $(find ${M3HOME} -maxdepth 1 -type d | grep -ve "${M3HOME}$") | head -n 1) | head" \
# about?
# Currently this script does not know the name of the executable it's building :-(
# TODO: 'read' bldit::MODEL @ runtime (e.g., write to a file?)
# But we can surmise that the executable will be among the newer (if not the newest) files in the newest subdir of M3HOME. So list those.

## why does `pushd` fail inside `eval` loops, but ONLY WHEN PIPING OUTPUT ???
echo -e "$ pushd ${CCTM_BUILD_DIR}" 2>&1 | tee -a "${LOG_PATH}"
pushd "${CCTM_BUILD_DIR}"

# build with loop lines, then
#  "ls -alt $(ls -1dt $(find ${CCTM_EXEC_DIR} -maxdepth 1 -type d | grep -ve "${CCTM_EXEC_DIR}$") | head -n 1) | head" \
# to find CCTM_EXEC
for CMD in \
  "${CCTM_BUILD_EXEC}" \
  "ls -al ${CCTM_EXEC}" \
  "ls -al ${LOG_PATH}" \
  "popd" \
; do
  echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_PATH}"
  eval "${CMD}" 2>&1 | tee -a "${LOG_PATH}"
  if [[ -n "$(tail -n 3 ${LOG_PATH} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
    echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_PATH}"
    exit 10
  fi
done

exit 0 # victory!

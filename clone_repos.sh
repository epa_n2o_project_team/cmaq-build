#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Clone all git repos currently used to build and benchmark CMAQ.
### For details see https://bitbucket.org/epa_n2o_project_team/cmaq-build
### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `date`
### * `dirname`
### * `ls`
### * `readlink`
### * `tail`
### * `tee`
### * `tr`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

## Get these out of the way before we get to work:
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
ERROR_PREFIX="${THIS_FN}: ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_FP="${THIS_DIR}/${LOG_FN}"

## Most important environment variables (envvars) for config.cmaq:
## see CMAQ-build/uber.config.cmaq.sh for details
UBER_CONFIG_CMAQ_FN='uber.config.cmaq.sh'
export UBER_CONFIG_CMAQ_FP="${THIS_DIR}/${UBER_CONFIG_CMAQ_FN}"

### TODO: allow uber_driver.sh to override all of the top-level constants:

if [[ -z "${M3HOME}" ]] ; then
#  source "${UBER_CONFIG_CMAQ_FP}" 2>&1 | tee -a "${LOG_FP}"
# cannot pipe and `source`?
  echo -e "$ source ${UBER_CONFIG_CMAQ_FP}" 2>&1 | tee -a "${LOG_FP}"
  source "${UBER_CONFIG_CMAQ_FP}"
fi

# copied from uber_driver::setup. TODO: refactor
if [[ -z "${M3HOME}" ]] ; then
  echo -e "${ERROR_PREFIX} root dir=M3HOME is not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
  exit 6
fi
if [[ -d "${M3HOME}" ]] ; then
  echo -e "${ERROR_PREFIX} root dir='${M3HOME}' exists: move or delete it before running this script"
  exit 7
else
  for CMD in \
    "mkdir -p ${M3HOME}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

## git-related
# SUPER_TAG='5.0.1_release_built_infinity_20130918' # desired `git` tag for repos
# EPA systems don't have SSL certificates, so lotsa password typing :-(
GIT_REMOTE_PREFIX='env GIT_SSL_NO_VERIFY=true'
# off EPA this should not be a problem (and is much preferred)
# GIT_REMOTE_PREFIX=''

## Repositories to clone.
# delimiter=\n for use with `for` loop

REPO_NAME_LIST='BLDMAKE
BLDMAKE-build
CCTM
CCTM-build
CMAQ-build
ICL
MECHS'
# not-yet-useful repos
# BCON
# ICON
# JPROC
# PARIO
# PROCAN
# STENEX
# TOOLS
# not-yet-created repos
# ICL-release
# MECHS-release

## URIs for cloning repos.
REPO_URI_PREFIX_GIT='git@bitbucket.org:tlroche/'
# REPO_URI_PREFIX_HTTP='https://tlroche@bitbucket.org/tlroche/'
# No! including username requires authentication! which requires typing on infinity :-(
REPO_URI_PREFIX_HTTP='https://bitbucket.org/epa_n2o_project_team/'
REPO_URI_SUFFIX_GIT='.git'
REPO_URI_SUFFIX_HTTP="${REPO_URI_SUFFIX_GIT}"

## Following are "booleans": comment out or change value if !TRUE
## Does our platform allow git/ssh? (e.g., EPA does not)
# CAN_GIT_SSH='TRUE'
## Does our platform have HTTP certificates? (e.g., EPA does not)
# HAVE_HTTP_CERTS='TRUE'

if [[ -n "${CAN_GIT_SSH}" || "${CAN_GIT_SSH}" == 'TRUE' ]] ; then
  ACCESS='git/ssh'          # preferred if available
  # git must not check for certificates
  export GIT_CLONE_PREFIX=''
  REPO_URI_PREFIX="${REPO_URI_PREFIX_GIT}"
  REPO_URI_SUFFIX="${REPO_URI_SUFFIX_GIT}"
else                        # cannot use protocol={git, ssh}
  REPO_URI_PREFIX="${REPO_URI_PREFIX_HTTP}"
  REPO_URI_SUFFIX="${REPO_URI_SUFFIX_HTTP}"
  if [[ -z "${HAVE_HTTP_CERTS}" || "${HAVE_HTTP_CERTS}" != 'TRUE' ]] ; then
    # set for ${DRIVER_FN}
    export GIT_CLONE_PREFIX='env GIT_SSL_NO_VERIFY=true'
    ACCESS='http-cert'  # ... if outgoing ssh blocked *and* no certs installed (e.g., EPA)
  else
    ACCESS='http+cert'  # ... if outgoing ssh blocked, but you have certificates
  fi # HAVE_HTTP_CERTS
fi # CAN_GIT_SSH

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### Takes args=
### 1. repo name
### 2. name of return variable (TODO: redo this in a real language.)
### Returns repo URI in ${name of return variable} (as suggested by http://www.linuxjournal.com/content/return-values-bash-functions )
function get_repo_URI {
  local REPO_NAME="${1}"
  local __ret_var_name="${2}"

  local REPO_ID="$(echo ${REPO_NAME} | tr '[:upper:]' '[:lower:]')"
#  local REPO_URI="${REPO_URI_PREFIX}${REPO_ID}${REPO_URI_SUFFIX}" # this fails to return to caller

#  echo -e "${THIS_FN}::${FUNCNAME[0]}: DEBUG: retvar name='${__ret_var_name}'"
#  echo -e "${THIS_FN}::${FUNCNAME[0]}: DEBUG: REPO_NAME='${REPO_NAME}'"

#  eval ${__ret_var_name}="'${REPO_URI}'" # this fails to return to caller
  eval ${__ret_var_name}="'${REPO_URI_PREFIX}${REPO_ID}${REPO_URI_SUFFIX}'" # returns
} # function get_repo_URI

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Don't proceed if ...

## ... we have no `git`
if [[ -z "$(which git)" ]] ; then
  echo -e "${ERROR_PREFIX} 'git' not available, exiting ..." 2>&1 | tee -a "${LOG_FP}"
  exit 4
fi

### Clone the repos

## why does `pushd` fail inside `eval` loops, but ONLY WHEN PIPING OUTPUT ???
echo -e "$ pushd ${M3HOME}" 2>&1 | tee -a "${LOG_FP}"
pushd "${M3HOME}"

## REPO_NAME is what *we* call a project,
## REPO_ID is whatever is needed for the URI,
## REPO_PATH is where (locally) we intend to put it
for REPO_NAME in ${REPO_NAME_LIST} ; do
#  echo -e "${THIS_FN}: DEBUG: REPO_NAME='${REPO_NAME}'"
  REPO_PATH="${M3HOME}/${REPO_NAME}"
  if [[ -d "${REPO_PATH}" ]] ; then
    for CMD in \
      "ls -al ${REPO_PATH}" \
    ; do
      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
        echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
        exit 5
      fi
    done # for CMD
    echo -e "${THIS_FN}: found REPO_PATH='${REPO_PATH}', skipping"

  else # clone the repo

#    REPO_URI="$(get_repo_URI ${REPO_NAME} 'REPO_URI')" # see function above
    # NO! we are not returning a value like a normal language :-(
    get_repo_URI "${REPO_NAME}" 'REPO_URI'
#    echo -e "${THIS_FN}: DEBUG: REPO_URI='${REPO_URI}'"

    for CMD in \
      "${GIT_REMOTE_PREFIX} git clone ${REPO_URI} ${REPO_PATH}" \
      "ls -al ${REPO_PATH}" \
    ; do
      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
        echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
        exit 5
      fi
    done # for CMD

    ## TODO: consume tag:
    #  * Checkout desired tag of CCTM-build
    #  * Recreate branch=master (after checkout-ing tag), otherwise we're detached HEAD (due to tag)
    #  1. Delete local branch=master: "${GIT_REMOTE_PREFIX} git checkout tags/${SUPER_TAG}"
    #  2. Delete remote branch=master ?: "${GIT_REMOTE_PREFIX} git branch -D master"
    #  3. Create local branch=master: "${GIT_REMOTE_PREFIX} git checkout -b master"
  fi # [[ -d "${REPO_PATH}" ]]
  echo # newline
done # for REPO_NAME
popd # from "${M3HOME}"

### What have we done?

for CMD in \
  "ls -al ${M3HOME}" \
  "ls -al ${LOG_FP}" \
; do
  echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
  if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
    echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
    exit 6
  fi
done # for CMD

exit 0 # victory!

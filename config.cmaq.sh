#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### bash version of CMAQ5.0.1 configuration script
### Mostly derived from   i:/home/yoj/src/repo/CCTM/config.cmaq
### With some values from i:/home/schwede/cctm/CCTM-git/config.cmaq

### This file must (until we get all-the-way off csh/tcsh) be co-maintained with
### * config.cmaq           (csh version of this file)
### * uber.config.cmaq.csh  (csh)
### * uber.config.cmaq.sh   (bash)

### Unlike the csh version, this does not auto-echo 

### The uber.config* set env vars that must be previously set: e.g., COMPILER, INFINITY, M3HOME, M3MODEL, M3LIB
### Other dependencies:
### * basename
### * cut
### * uname

### ----------------------------------------------------------------------

THIS="$0"
THIS_FN="$(basename ${THIS})"
# THIS_FN='CMAQ-build/config.cmaq.sh' # must set for source'ing?
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# csh version does 'set echo'
set -x # or -v ?

### architecture & compiler specific settings

export system="$(uname -i)"
export bld_os="$(uname -s)$(uname -r | cut -d. -f1)"

if   [[ -z "${INFINITY}" ]] ; then
  echo -e "${ERROR_PREFIX} INFINITY not defined, exiting ..."
  exit 2
elif [[ "${INFINITY}" == 'yes' ]] ; then
  export lib_basedir='/home/wdx/lib'
else
    ### for terra intel fortran compiler:
    ### module load intel-11.1 impi_3.2.2.006
  if [[ -z "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} M3HOME not defined, exiting ..."
    exit 2
  else
    export lib_basedir="${M3HOME}/lib"
  fi
fi

## Set envvar "compiler_ext" to accomdate Carlie Coats' ioapi naming convention, which does not follow the compiler directory name

case "${COMPILER}" in
  'intel' )
    export compiler='intel'
    export compiler_ext='ifort'
    export myFC='ifort'
    export myCC='icc'
    export myLINK_FLAG='-i-static'
    export  myFFLAGS='-fixed -132 -O3 -override-limits -fno-alias -mp1'
    export myFRFLAGS='-free       -O3                  -fno-alias -mp1'
    export myCFLAGS='-O2'
    export mpi='-lmpich'
    export extra_lib='-lmpiif'
    if [[ "${INFINITY}" == 'yes' ]] ; then
      INFI_INTEL_VERSION='11.1.059' # both icc and ifc # not available on amad1?
      export INFI_ICC_ROOT="/share/linux86_64/intel/cc/${INFI_INTEL_VERSION}/bin/intel64"
      export INFI_IFC_ROOT="/share/linux86_64/intel/fc/${INFI_INTEL_VERSION}/bin/intel64"
      export extra_lib='-lrdmacm -libumad -lrt' # from David Wong's scom
      export myCC="${INFI_ICC_ROOT}/icc"
      export myFC="${INFI_IFC_ROOT}/ifort"
    fi
    ;;

  'pgi' )
    ### for terra Portland Group fortran compiler:
    ### module load pgi-11.9 openmpi_pgi
    export compiler='pgi'
    export compiler_ext='pg'
    export LM_LICENSE_FILE='/usr/local/pgi/license.dat'
    export myFC='/usr/local/pgi-11.9/linux86-64/11.9/bin/pgf90'
    export myCC='/usr/local/pgi-11.9/linux86-64/11.9/bin/pgcc'
    export myLINK_FLAG=''
    export myFFLAGS='-Mfixed -Mextend -O3'
    export myFRFLAGS='-Mfree -O3'
    export myCFLAGS='-O2'
    export mpi='-lmpi'
    export extra_lib='-lmpi_f77 '
    ;;

  'gcc' )
    ### for terra gfortran compiler
    ### module load gcc-4.6 openmpi-1.4.3_gcc46
    export compiler='gcc'
    export compiler_ext='gfort'
    export myFC='/usr/local/gcc-4.6/bin/gfortran'
    export myCC='gcc'
    export myLINK_FLAG=''
    export myFFLAGS='-ffixed-form -ffixed-line-length-132 -O3 -funroll-loops -finit-character=32'
    export myFRFLAGS='-ffree-form -ffree-line-length-none -O3 -funroll-loops -finit-character=32'
    export myCFLAGS='-O2'
    export mpi='-lmpich'
    export extra_lib=''
    if [[ "${INFINITY}" == 'yes' ]] ; then
      export extra_lib="${extra_lib} -lgomp"
    fi
    ;;

  * )
    echo -e "${ERROR_PREFIX} cannot handle COMPILER='${COMPILER}', exiting ..."
    exit 2
    ;;

esac # case "${COMPILER}"

### TODO: track down the several versions of this information
export EXEC_ID="${bld_os}_${system}${compiler}"

### Set dependency locations
if [[ "${INFINITY}" == 'yes' ]] ; then
  # very subtle difference--------------v
  export M3LIB="${lib_basedir}/${system}i/${compiler}"
else
  export M3LIB="${lib_basedir}/${system}/${compiler}"
fi

## includes and libraries
NETCDF_DIR="${M3LIB}/netcdf/lib"
export NETCDF="-L${NETCDF_DIR} -lnetcdf"
export LNETCDF="${NETCDF}"
## I/O API
IOAPI_VERSION="3.1"
IOAPI_DIR="${M3LIB}/ioapi_${IOAPI_VERSION}/Linux2_x86_64ifort"
export IOAPIMOD="${IOAPI_DIR}"
export IOAPI="-L${IOAPI_DIR} -lioapi"
export LIOAPI="${IOAPI}"

### compilers, linkers
export CC="${myCC}"
export FC="${myFC}"
export CPP="${FC}"
export FP="${FC}"
export LINKER="${FC}"

### compile/link flags
export F_FLAGS="${myFFLAGS} -I ${IOAPIMOD} -I."
export f_FLAGS="${F_FLAGS}"
export F90_FLAGS="${myFRFLAGS} -I ${IOAPIMOD} -I."
export f90_FLAGS="${F90_FLAGS}"
export CPP_FLAGS=''
export C_FLAGS="${myCFLAGS} -DFLDMN"
export LINK_FLAGS="${myLINK_FLAG}"

### user choices:  single or multiple processors
export ParOpt='yes'             # set for multiprocessing; comment out otherwise

if [[ -n "${ParOpt}" ]] ; then  # parallel/multiprocessor configuration
 export PARIO="${M3LIB}/pario_${IOAPI_VERSION}"
 export STENEX="${M3LIB}/se_snl"
 export MPI_INC="${M3LIB}/mpich/include"

# note that results of
# me@infinity:~ $ find ${MPI_INC}/ -name '*.h' | xargs fgrep -nH -ie 'version' | fgrep -vie 'conversion'
# indicate use of MPICH2_VERSION=="1.4.0"==MVAPICH2_VERSION

 export MPI="/share/linux86_64/mvapich2/mvapich2-1.4/install_x86_64_intel111059/bin"
 export MPIRUN="$MPI/mpiexec"
else                            # serial/uniprocessor configuration
 export PARIO="."
 export STENEX="${M3LIB}/se_noop"
 export MPI_INC="."
fi

# csh version does 'unset echo'
set +x # or +v ?

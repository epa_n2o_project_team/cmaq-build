#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Copy filespec from local ASM dir/folder on EMVL to target dir/folder on remote HPCC. For each file
### 1. Un-ASM the file "in ASM space" with `aget`
### 2. `rsync` the unarchived file to HPCC
### Currently just copies all the files (via `rsync`). TODO: allow subsetting, filetrees, etc.
### Assumes SSH connectivity for user on target is {setup for, working on} remote.

### To be used by drivers that "fill-in" 4 required envvars:
### * ASM_DIR: absolute/FQ path to ASM {archive, source of files}.
### * TARGET_HOST: FQ name of remote host (or gateway).
### * TARGET_USER: account thereon. 
### * TARGET_DIR: absolute/FQ path on remote to which files will be copied.

### 2 envvars are optional:
### * ASM_FILESPEC: pattern for `find -name`. If specified, we will direct `aget` to NOT copy these filename patterns from ${SOURCE_DIR}.
### * SOURCE_EXCLUDE: if specified, we will direct `rsync` to NOT copy these paths (which MUST BE RELATIVE) from ${SOURCE_DIR}.
###   Best used to protect calling script from getting overwritten. E.g., in caller, do
###   `SOURCE_EXCLUDE="./${THIS_FN}"`
###   Can pass multiple exclude options, BUT ONLY AS STRING (since bash does not yet support export of arrays): see 'sample driver scripts' below.

### Files will be initially retrieved to ASM space per

### From: "Anderson, Edward" <Anderson.Edward@epa.gov>
### To: Tom Roche <Tom_Roche@pobox.com>, Emvl Help <Emvl.Help@epa.gov>
### Subject: RE: ASM-to-HPCC without intermediate write?
### Date: Thu, 14 Aug 2014 13:52:37 +0000
### References: <871tsj4520.fsf@pobox.com>
### In-Reply-To: <871tsj4520.fsf@pobox.com>
### Message-ID: <8b32a43142584cb9a535675f0d91f63c@BY2PR09MB0293.namprd09.prod.outlook.com>

### > When you do aget, you don't have to put the files onto /work where you are limited by your quota.  You can just do

### > aget /asm/directory/filename

### > and it will recall the file from tape to the /asm disk.

### See following clarification from Bob Whitney.

### > The only caveat is that there is no quota -- as the /asm disk fills up, your files may be released at any time, so you can't unmigrate, say, 20 TB of data and expect it to still be there tomorrow.  However, this should work if you recall files and scp or rsync them right away.

### > You can only run the aget command from login nodes, so to do it as a batch job you would need to use the singlepe job queue which has a 12-hour limit.  If that is not sufficient for you, let us know.

### From: "Whitney, Bob" <Whitney.Bob@epa.gov>
### To: Tom Roche <Tom_Roche@pobox.com>, "Anderson, Edward" <Anderson.Edward@epa.gov>
### CC: Emvl Help <Emvl.Help@epa.gov>
### Subject: RE: ASM-to-HPCC without intermediate write?
### Date: Fri, 15 Aug 2014 12:00:50 +0000
### References: <8b32a43142584cb9a535675f0d91f63c@BY2PR09MB0293.namprd09.prod.outlook.com> <871tsj4520.fsf@pobox.com> <87vbpu10n6.fsf@pobox.com>
### In-Reply-To: <87vbpu10n6.fsf@pobox.com>
### Message-ID: <ffdb76ee16d04923837a0263c37cac5f@BL2PR09MB0147.namprd09.prod.outlook.com>
### (excerpted)

### > Load balancing is a manual process that at time of user directory creation  is assigned to either ASM1 or ASM2.  Project directories are balanced between machines and generally which ever project a user belongs to that is the ASM where their ASM user directory resides.  When your data comes back from tape it is always to the same ASM where the data was created.

### > Your UID has been assigned to ASM1,  MOD3APP has been assigned to asm1.  For both, the data will always be on ASM1.   To determine which ASM an UID or PROJECT has been assigned do a "ls -l" on /asm and look for the symbolic link you were searching for.

### TODO:

### 1. Better determination of `aget`s target filepath:
### * (best) Get `aget`s target filepath *from `aget`*! Don't guess!
### * Parameterize target ASM drive='asm1' given above.

### 2. Deal with filename prefixes as used in the following (which therefore cannot currently call this script):
### * i:/project/inf35w/roche/BC/2008cdc/AQonly/get.sh
### * i:/project/inf35w/roche/IC/2008cdc/AQonly/get.sh
### * i:/project/inf35w/roche/JTABLE/get.sh

### 3. Exit after non-0 return from any pipeline stage.

### 4. Progress options:
### 4.1. notify user that `sdu` is long-running
### 4.2. progress feedback for the file transport, which is arbitrarily long-running!

### 5. Support options for --dry-run/--no-eval, --debug, etc.

### 6. Canonicalize (i.e., "my way" :-) payload -> loop.

# ----------------------------------------------------------------------
# constants and their testing
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### rsync details

TARGET_SSH_PREFIX="${TARGET_USER}@${TARGET_HOST}"
TARGET_PATH="${TARGET_SSH_PREFIX}:${TARGET_DIR}/" # TODO: better ensure trailing slash, no dups

## default RSYNC

# RSYNC_OPTIONS='-avh --stats --delete-after --append-verify -e ssh'
# infinity has `rsync --version`==2.6.8, knows no '--append-verify'
# RSYNC_OPTIONS='-avh --stats --delete-after --append -e ssh'
# --delete* deletes this script!
# RSYNC_OPTIONS='-avh --stats --append -e ssh'
# do we need --append?
RSYNC_OPTIONS='-avh --stats --append -e ssh'
# '--dry-run' == testing
# RSYNC_OPTIONS="--dry-run ${RSYNC_OPTIONS}"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# test arguments
# ----------------------------------------------------------------------

if [[ -z "${TARGET_USER}" ]] ; then
  echo -e "${ERROR_PREFIX} TARGET_USER undefined, exiting ..."
  exit 2
else
  echo -e "${MESSAGE_PREFIX} TARGET_USER='${TARGET_USER}'"
fi

if [[ -z "${TARGET_HOST}" ]] ; then
  echo -e "${ERROR_PREFIX} TARGET_HOST undefined, exiting ..."
  exit 3
else
  echo -e "${MESSAGE_PREFIX} TARGET_HOST='${TARGET_HOST}'"
fi

if [[ -z "${TARGET_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} TARGET_DIR undefined, exiting ..."
  exit 4
else
  echo -e "${MESSAGE_PREFIX} TARGET_DIR='${TARGET_DIR}'"
fi

if   [[ -z "${ASM_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} ASM_DIR undefined, exiting ..."
  exit 5
elif [[ ! -r "${ASM_DIR}" ]] ; then
  echo -e "ERROR: cannot read target dir='${ASM_DIR}', exiting ..."
  exit 6
else
  echo -e "${MESSAGE_PREFIX} ASM_DIR='${ASM_DIR}'"
fi

### Workaround lack of bash support for exporting arrays.
if   [[ -z "${SOURCE_EXCLUDE}" ]] ; then
  echo -e "${MESSAGE_PREFIX} RSYNC_OPTIONS='${RSYNC_OPTIONS}'"
else
  ### Convert passed "array" (which must be space-delimited string) into real array.
  eval "source_exclude=( ${SOURCE_EXCLUDE[*]} )"
  ## Parse potentially multiple excludes:
  for (( I=0; I<"${#source_exclude[@]}"; I++ )) ; do
    echo -e "${MESSAGE_PREFIX} SOURCE_EXCLUDE[${I}]='${source_exclude[${I}]}'"
    RSYNC_OPTIONS="${RSYNC_OPTIONS} --exclude '${source_exclude[${I}]}'"
  done
fi

if [[ -z "${ASM_FILESPEC}" ]] ; then
  ASM_FIND_CMD="find ${ASM_DIR}/"
else
  ASM_FIND_CMD="find ${ASM_DIR}/ -name '${ASM_FILESPEC}'"
fi

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

START="$(date)"
echo -e "${MESSAGE_PREFIX} START=${START}"

### TODO: exit after non-0 return from any pipeline stage

### Compute source stats: n(files) , du(files) , etc
### TODO: notify user that `sdu` is long-running
for CMD in \
  "${ASM_FIND_CMD} | wc -l" \
  "sdu -hc \$(${ASM_FIND_CMD} -print0 | tr '\0' ' ') | tail -n 1" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}" # for testing
done

### Ensure TARGET_DIR? `rsync` will do this for us
# for CMD in \
#   "mkdir -p ${TARGET_DIR}/" \
# ; do
#   CMD="ssh ${TARGET_SSH_PREFIX} '${CMD}'"
#   echo -e "${MESSAGE_PREFIX} ${CMD}"
#   eval "${CMD}"
# done

### TODO: provide (option for) progress for long-running jobs! You know how many files you're gonna copy ...
# for ASM_SOURCE_FP in '/asm/MOD3APP/F40/smoke_out/2008ab/12US1/cmaq_cb05/model_ready/emis_mole_all_20071223_12US1_cmaq_cb05_F40_2008ab.ag_v2.ncf' ; do # for testing
# for ASM_SOURCE_FP in $(eval ${ASM_FIND_CMD}) ; do
# That works, but the files are not sorted, which is currently our best progress feedback.
# So until we fix progress,
SORTED_ASM_FIND_CMD="${ASM_FIND_CMD} | sort"
for ASM_SOURCE_FP in $(eval ${SORTED_ASM_FIND_CMD}) ; do
#  echo -e "ASM_SOURCE_FP='${ASM_SOURCE_FP}'" # debugging
  # note: the `aget` syntax for in-ASM unarchiving! and added `date`s for pseudo-progress
  # * the `aget` syntax for in-ASM unarchiving!
  # * added `date`s for pseudo-progress
#  ASM_TARGET_FP="/tmp/${ASM_SOURCE_FP}"
#  `rsync` won't see the same /tmp :-(
  # So try to guess/specify where ASM wants to copy.
  # Empirically (n=1 :-) /asm/ -> /asm1/ , so
  ASM_TARGET_FP="$(echo -e "${ASM_SOURCE_FP}" | sed -e 's%^/asm/%/asm1/%')"
  ASM_TARGET_DIR="$(dirname ${ASM_TARGET_FP})"
  RSYNC_TARGET_DIR="${TARGET_SSH_PREFIX}:${TARGET_DIR}"
  for CMD in \
    "date" \
    "mkdir -p ${ASM_TARGET_DIR}/" \
    "aget -r ${ASM_TARGET_FP} ${ASM_SOURCE_FP}" \
    "rsync ${RSYNC_OPTIONS} ${ASM_TARGET_FP} ${RSYNC_TARGET_DIR}/" \
    "date" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
  echo # newline between each file
done # for ASM_SOURCE_FP

### Compute target stats:
for CMD in \
  "du -hs ${TARGET_DIR}/" \
  "find ${TARGET_DIR}/ -type f | wc -l" \
; do
  CMD="ssh ${TARGET_SSH_PREFIX} '${CMD}'"
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

echo -e "${MESSAGE_PREFIX}   END=$(date)"
echo -e "${MESSAGE_PREFIX} START=${START}"
exit 0 # need this to avoid following doc!

# ----------------------------------------------------------------------
# sample driver scripts (copied from ancestor, not yet verified!)
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# simplest driver
# ----------------------------------------------------------------------

# Doesn't pollute namespace, but comments are problematic.
# Relative target dir means driver must be *in* that dir/folder.
# "Our helper" == this file (or something else that calls it).
ASM_FILESPEC='*2008ab.ag_v2.ncf'
ASM_DIR='/asm/my/path' \
TARGET_USER='me' \
TARGET_HOST='evil.giant.com' \
TARGET_DIR='/path/to/runspace' \
/path/to/this.sh

# ----------------------------------------------------------------------
# protect only the caller
# ----------------------------------------------------------------------

# Uses `export`, which pollutes namespace, but comments are easy as usual.
THIS="$0"                                     # whatever is calling
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"  # FQ path to caller's $(pwd)
# THIS_DIR allows calling caller from any directory, rather than just ./caller
export ASM_FILESPEC='?CON*'
export ASM_DIR='/asm/another/path' \
export TARGET_USER='myself' \
export TARGET_HOST='harbor.virtuous.org'
export TARGET_DIR='/path/to/runspace' \
export SOURCE_EXCLUDE="${THIS_FN}"            # don't overwrite caller
/path/to/this.sh

# ----------------------------------------------------------------------
# multiple excludes
# ----------------------------------------------------------------------

THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
export ASM_FILESPEC='emis*.ncf'
export ASM_DIR='/asm/another/path' \
export TARGET_USER='that_guy' \
export TARGET_HOST='morally.ambiguous.net'
export TARGET_DIR='/path/to/runspace' \
### Note: bash does not yet support exporting arrays! so workaround:
source_exclude=("${THIS_FN}" '.git/')
### (TODO: delimit (and split in helper) with something less troublesome than ' ')
export SOURCE_EXCLUDE="$(printf '%q ' "${source_exclude[@]}")"
# ... will restore the array like `eval "SOURCE_EXCLUDE=( ${source_exclude[*]} )"`
/path/to/this.sh

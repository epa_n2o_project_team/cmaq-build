#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Copy all files from source dir/folder on {EMVL, HPCC} to target dir/folder on local {HPCC, EMVL}.
### Currently just copies all the files (via `rsync`). TODO: allow subsetting, filetrees, etc.
### Assumes SSH connectivity for user on target is {setup for, working on} remote.

### To be used by drivers that "fill-in" 4 required envvars:
### * SOURCE_HOST: FQ name of remote host (or gateway).
### * SOURCE_USER: account thereon. 
### * SOURCE_DIR:  absolute/FQ path on remote from where files will be copied.
### * TARGET_DIR:  relative or absolute path on local to which files will be copied.

### 1 envvar is optional:
### * SOURCE_EXCLUDE: if specified, we will direct `rsync` to NOT copy these paths (which MUST BE RELATIVE) from ${SOURCE_DIR}.
###   Best used to protect calling script from getting overwritten. E.g., in caller, do
###   `SOURCE_EXCLUDE="./${THIS_FN}"`
###   Can pass multiple exclude options, BUT ONLY AS STRING (since bash does not yet support export of arrays): see 'sample driver scripts' below.

### TODO:
### * Deal with filename prefixes as used in the following (which therefore cannot currently call this script):
### ** i:/project/inf35w/roche/BC/2008cdc/AQonly/get.sh
### ** i:/project/inf35w/roche/IC/2008cdc/AQonly/get.sh
### ** i:/project/inf35w/roche/JTABLE/get.sh

# ----------------------------------------------------------------------
# constants and their testing
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### rsync details

SOURCE_SSH_PREFIX="${SOURCE_USER}@${SOURCE_HOST}"
SOURCE_FQP="${SOURCE_SSH_PREFIX}:${SOURCE_DIR}/" # TODO: better ensure trailing slash
TARGET_PATH="${TARGET_DIR}/"  # TODO: better ensure trailing slash

## default RSYNC

# RSYNC_OPTIONS='-avh --stats --delete-after --append-verify -e ssh'
# infinity has `rsync --version`==2.6.8, knows no '--append-verify'
# RSYNC_OPTIONS='-avh --stats --delete-after --append -e ssh'
# --delete* deletes this script!
RSYNC_OPTIONS='-avh --stats --append -e ssh'
# '--dry-run' == testing
# RSYNC_OPTIONS='--dry-run -avh --stats --append -e ssh'

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# test arguments
# ----------------------------------------------------------------------

START="$(date)"
echo -e "${MESSAGE_PREFIX} START=${START}"

if [[ -z "${SOURCE_USER}" ]] ; then
  echo -e "${ERROR_PREFIX} SOURCE_USER undefined, exiting ..."
  exit 2
else
  echo -e "${MESSAGE_PREFIX} SOURCE_USER='${SOURCE_USER}'"
fi

if [[ -z "${SOURCE_HOST}" ]] ; then
  echo -e "${ERROR_PREFIX} SOURCE_HOST undefined, exiting ..."
  exit 3
else
  echo -e "${MESSAGE_PREFIX} SOURCE_HOST='${SOURCE_HOST}'"
fi

if [[ -z "${SOURCE_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} SOURCE_DIR undefined, exiting ..."
  exit 4
else
  echo -e "${MESSAGE_PREFIX} SOURCE_DIR='${SOURCE_DIR}'"
fi

if   [[ -z "${TARGET_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} TARGET_DIR undefined, exiting ..."
  exit 5
elif [[ ! -r "${TARGET_DIR}" ]] ; then
  echo -e "ERROR: cannot read target dir='${TARGET_DIR}', exiting ..."
  exit 6
else
  echo -e "${MESSAGE_PREFIX} TARGET_DIR='${TARGET_DIR}'"
fi

### Workaround lack of bash support for exporting arrays.
if   [[ -z "${SOURCE_EXCLUDE}" ]] ; then
  echo -e "${MESSAGE_PREFIX} RSYNC_OPTIONS='${RSYNC_OPTIONS}'"
else
  ### Convert passed "array" (which must be space-delimited string) into real array.
  eval "source_exclude=( ${SOURCE_EXCLUDE[*]} )"
  ## Parse potentially multiple excludes:
  for (( I=0; I<"${#source_exclude[@]}"; I++ )) ; do
    echo -e "${MESSAGE_PREFIX} SOURCE_EXCLUDE[${I}]='${source_exclude[${I}]}'"
    RSYNC_OPTIONS="${RSYNC_OPTIONS} --exclude '${source_exclude[${I}]}'"
  done
fi

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

for CMD in \
  "rsync ${RSYNC_OPTIONS} ${SOURCE_FQP} ${TARGET_PATH}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done
echo # newline

# compute source and target stats: n(files) , du(files) , etc
for DIR in "${SOURCE_DIR}" "${TARGET_DIR}" ; do
  for CMD in \
    "du -hs ${DIR}/" \
    "find ${DIR}/ -type f | wc -l" \
  ; do
    if [[ "${DIR}" == "${SOURCE_DIR}" ]] ; then
      CMD="ssh ${SOURCE_SSH_PREFIX} '${CMD}'"
    fi
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done # for CMD
  echo # newline
done # for DIR

echo -e "${MESSAGE_PREFIX}   END=$(date)"
echo -e "${MESSAGE_PREFIX} START=${START}"
exit 0 # need this to avoid following doc!

# ----------------------------------------------------------------------
# sample driver scripts
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# simplest driver
# ----------------------------------------------------------------------

# Doesn't pollute namespace, but comments are problematic.
# Relative target dir means driver must be *in* that dir/folder.
# "Our helper" == this file (or something else that calls it).
TARGET_DIR='.' \
SOURCE_USER='me' \
SOURCE_HOST='evil.giant.com' \
SOURCE_DIR='~/my/path' \
/path/to/our/helper.sh

# ----------------------------------------------------------------------
# protect only the caller
# ----------------------------------------------------------------------

# Uses `export`, which pollutes namespace, but comments are easy as usual.
THIS="$0"                                     # caller
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"  # FQ path to caller's $(pwd)
# THIS_DIR allows calling caller from any directory, rather than just ./caller
export SOURCE_USER='myself'
export SOURCE_HOST='harbor.virtuous.org'
export SOURCE_DIR="/another/path"
export TARGET_DIR="${THIS_DIR}"
export SOURCE_EXCLUDE="${THIS_FN}" # don't overwrite caller
/path/to/our/helper.sh

# ----------------------------------------------------------------------
# multiple excludes
# ----------------------------------------------------------------------

THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
export SOURCE_USER='I'
export SOURCE_HOST='morally.ambiguous.net'
export SOURCE_DIR="/yet/another/path"
export TARGET_DIR="${THIS_DIR}"

### Note: bash does not yet support exporting arrays! so workaround:
source_exclude=("${THIS_FN}" '.git/')
### (TODO: delimit (and split in helper) with something less troublesome than ' ')
export SOURCE_EXCLUDE="$(printf '%q ' "${source_exclude[@]}")"
### Our helper ...
/path/to/our/helper.sh
# ... will restore the array like `eval "SOURCE_EXCLUDE=( ${source_exclude[*]} )"`

#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Download/explode CMAQ-5.0.1 tarballs for the one-way (WRF->CMAQ) code and data
### Requires `tar` and `wget`.
### Requires
### * `find` (tested with version=4.2.27)
### * `tar` (tested with GNU version=1.15.1)
### * `wget` (tested with version=1.11.4 Red Hat modified)
### * `` (tested with version=)
### Assumes platform=linux.
### Best driven by ./repo_creator.sh (or set envvars as needed)

### TODO: take switch to either {download/explode to/from pipe, [download, explode]}

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS_FP="$0"
THIS_DIR="$(dirname ${THIS_FP})"
THIS_FN="$(basename ${THIS_FP})"
ERROR_PREFIX="${THIS_FN}: ERROR: "

### Use driver=./repo_creator.sh values, or edit following
# CMAQ_VERSION='5.0.1'
# CMAQ_STAMP="CMAQv${CMAQ_VERSION}"                # root of tarball
# DOWNLOAD_ROOT='/project/inf35w/roche/CMAQ-5.0.1' # on infinity
# TARBALL_DIR="${DOWNLOAD_ROOT}/tarballs"
# TARSPLAT_ROOT="${DOWNLOAD_ROOT}/tarsplat-transplant"
# TARSPLAT_DIR="${TARSPLAT_ROOT}/${CMAQ_STAMP}"

URI_PREFIX="ftp://ftp.unc.edu/pub/cmas/SOFTWARE2/MODELS/CMAQ/${CMAQ_VERSION}"
URI_SUFFIX="CMAQv${CMAQ_VERSION}.tar.gz"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### TODO: guard with test for switch to[download, explode]
if [[ -z "${TARSPLAT_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} undefined TARSPLAT_DIR, exiting."
  exit 2
fi

if [[ ! -d "${TARSPLAT_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${TARSPLAT_DIR}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
  if [[ ! -d "${TARSPLAT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to create tarsplat dir='${TARSPLAT_DIR}', exiting."
    exit 2
  fi
fi

if [[ "$(find ${TARSPLAT_DIR} -type f | wc -l)" != "0" ]] ; then
  echo -e "${THIS_FN}: found tarsplat (exploded tarballs), skipping [download, explode]:"
  for CMD in \
    "ls -alh ${TARSPLAT_DIR}" \
    "du -hs ${TARSPLAT_DIR}" \
    "find ${TARSPLAT_DIR} -type f | wc -l" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
  exit 0 # success?
fi

### We have no tarsplat.
### Do we have tarballs?
### TODO: guard with test for switch to[download, explode]
if [[ ! -d "${TARBALL_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${TARBALL_DIR}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

if [[ "$(find ${TARBALL_DIR} -type f | wc -l)" != "0" ]] ; then
  echo -e "${THIS_FN}: found tarballs, skipping download:"
  for CMD in \
    "ls -alh ${TARBALL_DIR}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
  echo # newline
else

  ### Download tarballs.
  for TARBALL_OUTPUT_DIR_NAME in \
    'data' \
    'models' \
    'scripts' \
  ; do
    TARBALL_OUTPUT_DIR="${TARSPLAT_ROOT}/${TARBALL_OUTPUT_DIR_NAME}"
    if [[ -d "${TARBALL_OUTPUT_DIR}" ]] ; then
      echo -e "${ERROR_PREFIX} move or delete TARBALL_OUTPUT_DIR='${TARBALL_OUTPUT_DIR}', exiting."
      exit 2
    else
    for CMD in \
      "mkdir -p ${TARBALL_OUTPUT_DIR}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
    fi
  done

  echo # newline
  START="$(date)"
  # echo -e "${THIS_FN}: START=${START} (download/explode tarballs)"
  echo -e "${THIS_FN}: START=${START} (download tarballs)"

  for ONEWAY_PART in '' 'DATA.' 'DATA_REF.' ; do
    URI="${URI_PREFIX}/${ONEWAY_PART}${URI_SUFFIX}"
    TARBALL_FN="$(basename ${URI})"

    ### Download/explode to/from pipe is efficient, but CMAS bandwidth can be waay too restricted, so ...
#     for CMD in \
#       "pushd ${TARSPLAT_ROOT}" \
#       "wget -O - ${URI} | tar xzf -" \
#       "popd" \
#       "ls -alh ${TARSPLAT_ROOT}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done

    ### ... instead, [download, explode]}
    TARBALL_FP="${TARBALL_DIR}/${TARBALL_FN}"
    for CMD in \
      "wget -O ${TARBALL_FP} ${URI}" \
      "ls -alh ${TARBALL_DIR}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
    echo # newline
  done # for ONEWAY_PART

  echo -e "${THIS_FN}:   END=$(date) (download tarballs)"
  echo -e "${THIS_FN}: START=${START}"
  echo # newline
fi # [[ "$(find ${TARBALL_DIR} -type f | wc -l)" != "0" ]]

if   [[ ! -d "${TARBALL_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} '${TARBALL_DIR}' does not exist, exiting."
  exit 3
fi

if [[ "$(find ${TARBALL_DIR} -type f | wc -l)" == "0" ]] ; then
  echo -e "${ERROR_PREFIX} found no tarballs @ '${TARBALL_DIR}', exiting."
  exit 4
fi

### We have tarballs, but no tarsplat. So explode tarballs:
START="$(date)"
echo -e "${THIS_FN}: START=${START} (explode tarballs)"

for CMD in \
  "pushd ${TARSPLAT_ROOT}" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

for TARBALL_FP in $(find ${TARBALL_DIR} -type f) ; do
  echo -e "${THIS_FN}: exploding tarball='${TARBALL_FP}':"

  ### TODO: async explode in separate process, waiting for files
  if [[ -r "${TARBALL_FP}" ]] ; then
    for CMD in \
      "tar xzf ${TARBALL_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  else
    echo -e "${ERROR_PREFIX} cannot explode tarball='${TARBALL_FP}', exiting."
    exit 5
  fi

  for TOP_LEAF_DIR in $(find ${TARSPLAT_ROOT}/ -maxdepth 1 -type d | grep -ve '/$' | sort) ; do
    for CMD in \
      "ls -alh ${TOP_LEAF_DIR}" \
      "du -hs ${TOP_LEAF_DIR}" \
      "find ${TOP_LEAF_DIR} -type f | wc -l" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  done
  echo # newline
done # for TARBALL_FP

for CMD in \
  "popd" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

# echo -e "${THIS_FN}:   END=$(date) (download/explode tarballs)"
echo -e "${THIS_FN}:   END=$(date) (explode tarballs)"
echo -e "${THIS_FN}: START=${START}"
exit 0 # success!

# ----------------------------------------------------------------------
# example output
# ----------------------------------------------------------------------

### 17 Oct 13 on infinity, tarballs previously cached (`ls` excerpted):

# me@it:~ $ /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/get_one_way_tarballs.sh
# $ mkdir -p /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# get_one_way_tarballs.sh: found tarballs, skipping download:
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/tarballs
# -rw-r--r-- 1 roche mod3 3.6M Oct 17 19:35 CMAQv5.0.1.tar.gz
# -rw-r--r-- 1 roche mod3 1.4G Oct 17 19:39 DATA.CMAQv5.0.1.tar.gz
# -rw-r--r-- 1 roche mod3 6.6G Oct 17 19:46 DATA_REF.CMAQv5.0.1.tar.gz

# get_one_way_tarballs.sh: START=Thu Oct 17 21:15:19 EDT 2013 (explode tarballs)
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant

# get_one_way_tarballs.sh: exploding tarball='/project/inf35w/roche/CMAQ-5.0.1/tarballs/CMAQv5.0.1.tar.gz':
# $ tar xzf /project/inf35w/roche/CMAQ-5.0.1/tarballs/CMAQv5.0.1.tar.gz
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# drwxr-xr-x 13 roche mod3  140 Jul  6  2012 models
# drwxr-xr-x 14 roche mod3 4.0K Jul 11  2012 scripts
# $ du -hs /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# 25M	/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# $ find /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1 -type f | wc -l
# 1399

# get_one_way_tarballs.sh: exploding tarball='/project/inf35w/roche/CMAQ-5.0.1/tarballs/DATA.CMAQv5.0.1.tar.gz':
# $ tar xzf /project/inf35w/roche/CMAQ-5.0.1/tarballs/DATA.CMAQv5.0.1.tar.gz
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# drwxr-xr-x 14 roche mod3  146 Oct 17 21:16 data
# drwxr-xr-x 13 roche mod3  140 Jul  6  2012 models
# drwxr-xr-x 14 roche mod3 4.0K Jul 11  2012 scripts
# $ du -hs /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# 4.2G	/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# $ find /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1 -type f | wc -l
# 1532

# get_one_way_tarballs.sh: exploding tarball='/project/inf35w/roche/CMAQ-5.0.1/tarballs/DATA_REF.CMAQv5.0.1.tar.gz':
# $ tar xzf /project/inf35w/roche/CMAQ-5.0.1/tarballs/DATA_REF.CMAQv5.0.1.tar.gz
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# drwxr-xr-x 15 roche mod3  156 Oct 17 21:16 data
# drwxr-xr-x 13 roche mod3  140 Jul  6  2012 models
# drwxr-xr-x 14 roche mod3 4.0K Jul 11  2012 scripts
# $ du -hs /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# 13G	/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1
# $ find /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1 -type f | wc -l
# 1558

# $ popd
# get_one_way_tarballs.sh:   END=Thu Oct 17 21:19:08 EDT 2013 (explode tarballs)
# get_one_way_tarballs.sh: START=Thu Oct 17 21:15:19 EDT 2013

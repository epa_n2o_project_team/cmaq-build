#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Convert 'tarsplat' (exploded former tarballs) for one-way (WRF->CMAQ) CMAQ-5.0.1 code and data
### (which are mostly CVS repositories) to .git repositories for the "real" components.
### Requires
### * `bash` sufficiently new to do string manipulation (tested with version=3.2.25)
### * `cvs` (tested with version=1.11.22)
### * `find` (tested with version=4.2.27)
### * `git` (tested with version=1.7.9.2)
### * `readlink` (tested with version=5.97)
### * `` (tested with version=)
### Assumes platform=linux.

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS_FP="$0"
THIS_DIR="$(dirname ${THIS_FP})"
THIS_FN="$(basename ${THIS_FP})"
ERROR_PREFIX="${THIS_FN}: ERROR: "

### Use driver=./repo_creator.sh values, or edit following
# DOWNLOAD_ROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat' # on infinity
# CMAQ_VERSION='5.0.1'
# CMAQ_STAMP="CMAQv${CMAQ_VERSION}"
# TARSPLAT_ROOT="${DOWNLOAD_ROOT}/tarsplat-transplant"
# TARSPLAT_DIR="${TARSPLAT_ROOT}/${CMAQ_STAMP}"
# BUILDER_REPO_DIR='/project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build'
# GITIGNORE_STUB_FP="${BUILDER_REPO_DIR}/stub.gitignore"
# REPO_ROOT='/project/inf35w/roche/CMAQ-5.0.1/git/test'
# CVS_TAG="${CMAQ_STAMP//./_}" # used in tarball CVS repos

MODELS_ROOT="${TARSPLAT_DIR}/models"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function export_CVS_module {
  local START="$(date)"
  echo -e "START=${START}"

  # `cvs -q` and `cvs -Q` both refuse to not tell me what they've checked out
  # and even with `> /dev/null` gotta tell me the dirs they've updated
#    "ls -alh ${EXPORT_DIR}/" \
  for CMD in \
    "pushd ${REPO_ROOT}" \
    "cvs export -r ${CVS_TAG} -d ${EXPORT_DIR} ${CVS_MODULE_NAME} 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '" \
    "popd" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done

  echo -e "  END=$(date)"
  echo -e "START=${START}"
} # function export_CVS_module

function copy_additional_files {
  ## copy additional files we want in the repos
  # `cp -H`==copy contents of symlinks? WRONG!
  # use `readlink -f` to copy contents of symlinks
  BUILDER_REPO_ADDONS_DIR="${BUILDER_REPO_DIR}/${REPO_NAME}"
  for SOURCE_FP in $(find ${BUILDER_REPO_ADDONS_DIR}/ -maxdepth 1 | grep -ve '/$') ; do
    # ${SOURCE_FP} may or may not be a symlink
    FN="$(basename ${SOURCE_FP})"
    TRUE_SOURCE_FP="$(readlink -f ${SOURCE_FP})"
    TARGET_FP="${REPO_DIR}/${FN}"
    for CMD in \
      "cp ${TRUE_SOURCE_FP} ${TARGET_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  done
} # function copy_additional_files

function CVS_repo_to_git_repo {
  ## actually create the (local) repos
  # TODO: add `du` subtracting .git/
  for CMD in \
    "pushd ${REPO_DIR}" \
    "git init" \
    "popd" \
    "ls -alh ${REPO_DIR}/" \
    "find ${REPO_DIR} -type d | fgrep -ve '.git' | wc -l" \
    "find ${REPO_DIR} -type f | fgrep -ve '.git' | wc -l" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
} # function CVS_repo_to_git_repo

function unmanaged_code_to_git_repo {
  if [[ -d "${SOURCE_DIR}" ]] ; then
    echo -e "${THIS_FN}: attempting to create '${REPO_NAME}' from code in '${SOURCE_DIR}'"

    # remove the update_release management artifact
    for CMD in \
      "ls -alh ${SOURCE_DIR}/" \
      "mkdir -p ${REPO_DIR}" \
      "cp -r ${SOURCE_DIR}/* ${REPO_DIR}/" \
      "rm ${REPO_DIR}/update_release" \
      'copy_additional_files' \
      'CVS_repo_to_git_repo' \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
    echo # newline
  else # [[ ! -d "${SOURCE_DIR}" ]]
    echo -e "${ERROR_PREFIX} cannot find source dir='${SOURCE_DIR}' for repo='${REPO_NAME}', exiting."
    exit 5
  fi
} # function unmanaged_code_to_git_repo

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

if [[ -z "${REPO_ROOT}" ]] ; then
  echo -e "${ERROR_PREFIX} undefined REPO_ROOT, exiting."
  exit 2
elif [[ ! -d "${REPO_ROOT}" ]] ; then
  for CMD in \
    "mkdir -p ${REPO_ROOT}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

if [[ -z "${TARSPLAT_ROOT}" ]] ; then
  echo -e "${ERROR_PREFIX} undefined TARSPLAT_ROOT, exiting."
  exit 3
elif [[ ! -d "${TARSPLAT_ROOT}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot find input root='${TARSPLAT_ROOT}', exiting."
  exit 4
fi

### CVS-managed code -> git repos

for CVSROOT_PATH in $(find "${TARSPLAT_ROOT}" -type d -name 'CVSROOT' | sort) ; do

  ## export CVS

  export CVSROOT="$(dirname ${CVSROOT_PATH})"
  CVS_MODULE_NAME="$(basename ${CVSROOT})"
  if [[ "${CVS_MODULE_NAME}" == 'BUILD' ]] ; then # they're just different :-(
    REPO_NAME='BLDMAKE'
    EXPORT_DIR="${REPO_ROOT}/${REPO_NAME}"
    REPO_DIR="${EXPORT_DIR}/bldmake" # artifact of CMAS tarballs
  else
    REPO_NAME="${CVS_MODULE_NAME}"
    EXPORT_DIR="${REPO_ROOT}/${REPO_NAME}"
    REPO_DIR="${EXPORT_DIR}"
  fi

#  echo -e "CVSROOT='${CVSROOT}'"
  echo -e "${THIS_FN}: attempting to create '${REPO_NAME}' from CVSROOT='${CVSROOT}'"
  export_CVS_module
  export CVSROOT=''

  ## create git repos

  if [[ "${CVS_MODULE_NAME}" == 'BUILD' ]] ; then # they're just different :-(
    for CMD in \
      "cp -r ${REPO_DIR}/* ${EXPORT_DIR}/" \
      "rm -fr ${REPO_DIR}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
    REPO_DIR="${EXPORT_DIR}"
  fi
  copy_additional_files
  CVS_repo_to_git_repo

  echo # newline

done # for CVSROOT_PATH in

### Create git repos for some non-CVS-managed code? Not unless needed

# # Each NON_CVS_NAME has the form '<path from MODELS_ROOT>|<repository name>'
# for NON_CVS_NAME in \
#   'includes/release|ICL-release' \
#   'mechs/release|MECHS-release' \
# ; do
#   SOURCE_NAME="${NON_CVS_NAME%|*}"
# #  echo -e "SOURCE_NAME=${SOURCE_NAME}"
#   SOURCE_DIR="${MODELS_ROOT}/${SOURCE_NAME}"
#   REPO_NAME="${NON_CVS_NAME#*|}"
# #  echo -e "REPO_NAME=${REPO_NAME}"
#   REPO_DIR="${REPO_ROOT}/${REPO_NAME}"
#   unmanaged_code_to_git_repo
# done

# ----------------------------------------------------------------------
# test harness
# ----------------------------------------------------------------------

# TEST_TO_ROOT='/project/inf35w/roche/CMAQ-5.0.1/git/new'
# ROOT_TO_TEST='/project/inf35w/roche/CMAQ-5.0.1/git/test'
# for PATH_TO_TEST in $(find ${ROOT_TO_TEST}/ -maxdepth 1 -type d | grep -ve '/$') ; do
# #  echo -e "PATH_TO_TEST=${PATH_TO_TEST}"
#   TLL_NAME="$(basename ${PATH_TO_TEST})"
# #  echo -e "TLL_NAME=${TLL_NAME}"
#   TEST_TO_PATH="${TEST_TO_ROOT}/${TLL_NAME}"

#   for CMD in \
#     "diff -wB ${TEST_TO_PATH}/ ${PATH_TO_TEST}/ | grep -ve '^Common subdirectories:'" \
#   ; do
#     echo -e "$ ${CMD}"
#   done

#   if [[ "$(${CMD} | wc -l)" != "0" ]] ; then
#     eval "${CMD}"
#   fi
#   echo # newline
# done

# ----------------------------------------------------------------------
# example output
# ----------------------------------------------------------------------

# from 17 Oct 13 on infinity (run under repo_creator.sh, some useless output removed)

# $ /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/one_way_tarsplat_to_git.sh
# one_way_tarsplat_to_git.sh: attempting to create 'BCON' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/BCON'
# START=Thu Oct 17 21:50:21 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON BCON 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:22 EDT 2013
# START=Thu Oct 17 21:50:21 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BCON/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON/
# drwxr-xr-x 2 roche mod3   76 Oct 17  2013 bldmake
# drwxr-xr-x 2 roche mod3 4.0K Oct 17  2013 common
# drwxr-xr-x 7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r-- 1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x 2 roche mod3  115 Oct 17  2013 m3conc
# drwxr-xr-x 5 roche mod3   67 Oct 17  2013 prof_data
# drwxr-xr-x 2 roche mod3   66 Oct 17  2013 profile
# -rw-r--r-- 1 roche mod3  999 Oct 17  2013 README.md
# drwxr-xr-x 2 roche mod3   42 Oct 17  2013 tracer
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON -type d | fgrep -ve '.git' | wc -l
# 10
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/BCON -type f | fgrep -ve '.git' | wc -l
# 32

# one_way_tarsplat_to_git.sh: attempting to create 'BLDMAKE' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/BUILD'
# START=Thu Oct 17 21:50:22 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE BUILD 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:23 EDT 2013
# START=Thu Oct 17 21:50:22 EDT 2013
# $ cp -r /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/bldmake/* /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/
# $ rm -fr /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/bldmake
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BLDMAKE/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BLDMAKE/bldmake.f /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/bldmake.f
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BLDMAKE/cfg_module.f /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/cfg_module.f
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BLDMAKE/parser.f /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/parser.f
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BLDMAKE/utils.f /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/utils.f
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/BLDMAKE/.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE/
# -rw-r--r-- 1 roche mod3  24K Oct 17  2013 bldmake.f
# -rwxr-xr-x 1 roche mod3  23K Oct 17  2013 bldmake.F
# -rw-r--r-- 1 roche mod3  40K Oct 17  2013 cfg_module.f
# -rwxr-xr-x 1 roche mod3  35K Oct 17  2013 cfg_module.F
# drwxr-xr-x 7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r-- 1 roche mod3  107 Oct 17  2013 .gitignore
# -rwxr-xr-x 1 roche mod3  303 Oct 17  2013 Makefile.gfortran
# -rwxr-xr-x 1 roche mod3  291 Oct 17  2013 Makefile.ifort
# -rwxr-xr-x 1 roche mod3  298 Oct 17  2013 Makefile.pgf90
# -rw-r--r-- 1 roche mod3  11K Oct 17  2013 parser.f
# -rwxr-xr-x 1 roche mod3 9.6K Oct 17  2013 parser.F
# -rw-r--r-- 1 roche mod3  995 Oct 17  2013 README.md
# -rw-r--r-- 1 roche mod3 8.5K Oct 17  2013 utils.f
# -rwxr-xr-x 1 roche mod3 5.9K Oct 17  2013 utils.F
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE -type d | fgrep -ve '.git' | wc -l
# 1
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/BLDMAKE -type f | fgrep -ve '.git' | wc -l
# 12

# one_way_tarsplat_to_git.sh: attempting to create 'CCTM' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/CCTM'
# START=Thu Oct 17 21:50:23 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM CCTM 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:29 EDT 2013
# START=Thu Oct 17 21:50:23 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/CCTM/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM/
# drwxr-xr-x  5 roche mod3   45 Oct 17 21:50 aero
# drwxr-xr-x  3 roche mod3   18 Oct 17 21:50 biog
# drwxr-xr-x  5 roche mod3   69 Oct 17 21:50 cloud
# drwxr-xr-x  4 roche mod3   38 Oct 17 21:50 couple
# drwxr-xr-x  4 roche mod3   33 Oct 17 21:50 depv
# drwxr-xr-x  4 roche mod3   27 Oct 17 21:50 driver
# drwxr-xr-x  3 roche mod3   17 Oct 17 21:50 emis
# drwxr-xr-x 10 roche mod3  144 Oct 17 21:50 gas
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x  3 roche mod3   22 Oct 17 21:50 grid
# drwxr-xr-x  3 roche mod3   17 Oct 17  2013 hadv
# drwxr-xr-x  3 roche mod3   23 Oct 17  2013 hdiff
# drwxr-xr-x  3 roche mod3   17 Oct 17  2013 init
# drwxr-xr-x  4 roche mod3   31 Oct 17  2013 par
# drwxr-xr-x  4 roche mod3   41 Oct 17  2013 phot
# drwxr-xr-x  3 roche mod3   18 Oct 17  2013 plrise
# drwxr-xr-x  3 roche mod3   15 Oct 17  2013 procan
# -rw-r--r--  1 roche mod3 1000 Oct 17  2013 README.md
# drwxr-xr-x  4 roche mod3   48 Oct 17  2013 spcs
# drwxr-xr-x  3 roche mod3   17 Oct 17  2013 util
# drwxr-xr-x  4 roche mod3   27 Oct 17  2013 vadv
# drwxr-xr-x  4 roche mod3   31 Oct 17  2013 vdiff
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM -type d | fgrep -ve '.git' | wc -l
# 60
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/CCTM -type f | fgrep -ve '.git' | wc -l
# 363

# one_way_tarsplat_to_git.sh: attempting to create 'ICON' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/ICON'
# START=Thu Oct 17 21:50:29 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON ICON 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:30 EDT 2013
# START=Thu Oct 17 21:50:29 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/ICON/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON/
# drwxr-xr-x  2 roche mod3   76 Oct 17  2013 bldmake
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 common
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x  2 roche mod3  141 Oct 17  2013 m3conc
# drwxr-xr-x  2 roche mod3  103 Oct 17  2013 par
# drwxr-xr-x  5 roche mod3   67 Oct 17  2013 prof_data
# drwxr-xr-x  2 roche mod3   66 Oct 17  2013 profile
# -rw-r--r--  1 roche mod3  994 Oct 17  2013 README.md
# drwxr-xr-x  2 roche mod3   42 Oct 17  2013 tracer
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON -type d | fgrep -ve '.git' | wc -l
# 11
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/ICON -type f | fgrep -ve '.git' | wc -l
# 41

# one_way_tarsplat_to_git.sh: attempting to create 'ICL' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/includes/ICL'
# START=Thu Oct 17 21:50:30 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL ICL 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:31 EDT 2013
# START=Thu Oct 17 21:50:30 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/ICL/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL/
# drwxr-xr-x 2 roche mod3   27 Oct 17  2013 bin
# drwxr-xr-x 6 roche mod3   57 Oct 17  2013 fixed
# drwxr-xr-x 7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r-- 1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x 4 roche mod3   29 Oct 17  2013 procan
# -rw-r--r-- 1 roche mod3 1017 Oct 17  2013 README.md
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL -type d | fgrep -ve '.git' | wc -l
# 10
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL -type f | fgrep -ve '.git' | wc -l
# 13

# one_way_tarsplat_to_git.sh: attempting to create 'JPROC' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/JPROC'
# START=Thu Oct 17 21:50:31 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC JPROC 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:32 EDT 2013
# START=Thu Oct 17 21:50:31 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/JPROC/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC/
# drwxr-xr-x 3 roche mod3  24 Oct 17  2013 driver
# drwxr-xr-x 7 roche mod3 111 Oct 17  2013 .git
# -rw-r--r-- 1 roche mod3  75 Oct 17  2013 .gitignore
# -rw-r--r-- 1 roche mod3 995 Oct 17  2013 README.md
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC -type d | fgrep -ve '.git' | wc -l
# 3
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/JPROC -type f | fgrep -ve '.git' | wc -l
# 25

# one_way_tarsplat_to_git.sh: attempting to create 'MECHS' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/mechs/MECHS'
# START=Thu Oct 17 21:50:32 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS MECHS 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:34 EDT 2013
# START=Thu Oct 17 21:50:32 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/MECHS/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS/
# drwxr-xr-x  2 roche mod3   27 Oct 17  2013 bin
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05cl_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05tucl_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05tucl_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05tump_ae6_aq
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# -rw-r--r--  1 roche mod3 1.1K Oct 17  2013 README.md
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc07tb_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc07tc_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc99_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc99_ae6_aq
# drwxr-xr-x  2 roche mod3   35 Oct 17  2013 trac0
# drwxr-xr-x  2 roche mod3   35 Oct 17  2013 trac1
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS -type d | fgrep -ve '.git' | wc -l
# 12
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS -type f | fgrep -ve '.git' | wc -l
# 84

# one_way_tarsplat_to_git.sh: attempting to create 'PARIO' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/PARIO'
# START=Thu Oct 17 21:50:34 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO PARIO 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:35 EDT 2013
# START=Thu Oct 17 21:50:34 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/PARIO/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO/
# -rwxr-xr-x  1 roche mod3  756 Apr 19  2012 alloc_data_mod.f
# -rwxr-xr-x  1 roche mod3 6.3K Apr 19  2012 boundary.f
# -rwxr-xr-x  1 roche mod3 6.1K Apr 19  2012 get_write_map.f
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# -rwxr-xr-x  1 roche mod3 3.3K Apr 19  2012 growbuf.f
# -rwxr-xr-x  1 roche mod3 5.3K Apr 19  2012 gtndxhdv.f
# -rwxr-xr-x  1 roche mod3 4.8K Apr 19  2012 interpol.f
# -rwxr-xr-x  1 roche mod3 1.2K Apr 19  2012 Makefile
# -rwxr-xr-x  1 roche mod3 8.5K Apr 19  2012 parutilio.f
# -rwxr-xr-x  1 roche mod3  18K Apr 19  2012 pinterpb.f
# -rwxr-xr-x  1 roche mod3 4.0K Apr 19  2012 pinterpb_mod.f
# -rwxr-xr-x  1 roche mod3 2.3K Apr 19  2012 PIOGRID.EXT
# -rwxr-xr-x  1 roche mod3  14K Apr 19  2012 pio_init.f
# -rwxr-xr-x  1 roche mod3 2.9K Apr 19  2012 piomaps_mod.f
# -rwxr-xr-x  1 roche mod3  14K Apr 19  2012 pio_re_init.f
# -rwxr-xr-x  1 roche mod3 1.3K Apr 19  2012 PIOVARS.EXT
# -rwxr-xr-x  1 roche mod3 5.2K Apr 19  2012 pm3err.f
# -rwxr-xr-x  1 roche mod3 5.7K Apr 19  2012 pm3exit.f
# -rwxr-xr-x  1 roche mod3 2.7K Apr 19  2012 pm3warn.f
# -rwxr-xr-x  1 roche mod3 2.9K Apr 19  2012 pshut3.f
# -rwxr-xr-x  1 roche mod3  12K Apr 19  2012 ptrwrite3.f
# -rwxr-xr-x  1 roche mod3  12K Apr 19  2012 pwrgrdd.f
# -rwxr-xr-x  1 roche mod3  12K Apr 19  2012 pwrite3.f
# -rwxr-xr-x  1 roche mod3 7.2K Apr 19  2012 readbndy.f
# -rw-r--r--  1 roche mod3  979 Oct 17  2013 README.md
# -rwxr-xr-x  1 roche mod3 8.2K Apr 19  2012 subdmap.f
# -rwxr-xr-x  1 roche mod3 2.5K Apr 19  2012 wrsubmap.f
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO -type d | fgrep -ve '.git' | wc -l
# 1
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/PARIO -type f | fgrep -ve '.git' | wc -l
# 26

# one_way_tarsplat_to_git.sh: attempting to create 'PROCAN' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/PROCAN'
# START=Thu Oct 17 21:50:35 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN PROCAN 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:36 EDT 2013
# START=Thu Oct 17 21:50:35 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/PROCAN/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN/
# drwxr-xr-x  2 roche mod3  102 Oct 17  2013 data
# drwxr-xr-x  3 roche mod3   15 Oct 17  2013 driver
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x  3 roche mod3   21 Oct 17  2013 icl
# -rw-r--r--  1 roche mod3 1003 Oct 17  2013 README.md
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN -type d | fgrep -ve '.git' | wc -l
# 6
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/PROCAN -type f | fgrep -ve '.git' | wc -l
# 49

# one_way_tarsplat_to_git.sh: attempting to create 'STENEX' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/STENEX'
# START=Thu Oct 17 21:50:36 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX STENEX 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:37 EDT 2013
# START=Thu Oct 17 21:50:36 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/STENEX/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX/
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 noop_f90
# -rw-r--r--  1 roche mod3  979 Oct 17  2013 README.md
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 se_snl
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX -type d | fgrep -ve '.git' | wc -l
# 3
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/STENEX -type f | fgrep -ve '.git' | wc -l
# 40

# one_way_tarsplat_to_git.sh: attempting to create 'TOOLS' from CVSROOT='/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/TOOLS'
# START=Thu Oct 17 21:50:37 EDT 2013
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test
# $ cvs export -r CMAQv5_0_1 -d /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS TOOLS 2>&1 | fgrep -ve 'export: Updating' | grep -ve '^U '
# $ popd
#   END=Thu Oct 17 21:50:38 EDT 2013
# START=Thu Oct 17 21:50:37 EDT 2013
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/TOOLS/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS/README.md
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS/.gitignore
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS/
# drwxr-xr-x  2 roche mod3   59 Oct 17  2013 airs2ext
# drwxr-xr-x  2 roche mod3   74 Oct 17  2013 cast2ext
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 combine
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 mcip2arl
# drwxr-xr-x  2 roche mod3  115 Oct 17  2013 rd_airs
# -rw-r--r--  1 roche mod3  944 Oct 17  2013 README.md
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 sitecmp
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS -type d | fgrep -ve '.git' | wc -l
# 7
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/TOOLS -type f | fgrep -ve '.git' | wc -l
# 60

# one_way_tarsplat_to_git.sh: attempting to create 'ICL-release' from code in '/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/includes/release'
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/includes/release/
# -rwxr-xr-x 1 roche mod3 5.4K Apr 19  2012 CONST.EXT
# -rwxr-xr-x 1 roche mod3  832 Apr 19  2012 EMISPRM.EXT
# -rwxr-xr-x 1 roche mod3  13K Apr 19  2012 FILES_CTM.EXT
# drwxr-xr-x 2 roche mod3   57 Apr 19  2012 pa_noop
# -rwxr-xr-x 1 roche mod3  20K Apr 19  2012 PE_COMM.EXT
# -rwxr-xr-x 1 roche mod3 1.6K Apr 19  2012 update_release
# $ mkdir -p /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release
# $ cp -r /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/includes/release/* /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release/
# $ rm /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release/update_release
# $ copy_additional_files
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release/.gitignore
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/ICL-release/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release/README.md
# $ CVS_repo_to_git_repo
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release/
# -rwxr-xr-x  1 roche mod3 5.4K Oct 17  2013 CONST.EXT
# -rwxr-xr-x  1 roche mod3  832 Oct 17  2013 EMISPRM.EXT
# -rwxr-xr-x  1 roche mod3  13K Oct 17  2013 FILES_CTM.EXT
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# drwxr-xr-x  2 roche mod3   57 Oct 17  2013 pa_noop
# -rwxr-xr-x  1 roche mod3  20K Oct 17  2013 PE_COMM.EXT
# -rw-r--r--  1 roche mod3 1016 Oct 17  2013 README.md
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release -type d | fgrep -ve '.git' | wc -l
# 2
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/ICL-release -type f | fgrep -ve '.git' | wc -l
# 8

# one_way_tarsplat_to_git.sh: attempting to create 'MECHS-release' from code in '/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/mechs/release'
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/mechs/release/
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 cb05cl_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 cb05tucl_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 cb05tucl_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 cb05tump_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 saprc07tb_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 saprc07tc_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 saprc99_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Apr 19  2012 saprc99_ae6_aq
# drwxr-xr-x  2 roche mod3   35 Apr 19  2012 trac0
# -rwxr-xr-x  1 roche mod3 1.4K Apr 19  2012 update_release
# $ mkdir -p /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release
# $ cp -r /project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/models/mechs/release/* /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release/
# $ rm /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release/update_release
# $ copy_additional_files
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/stub.gitignore /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release/.gitignore
# $ cp /project/inf35w/roche/CMAQ-5.0.1/git/new/CMAQ-build/MECHS-release/README.md /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release/README.md
# $ CVS_repo_to_git_repo
# $ pushd /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release
# $ git init
# $ popd
# $ ls -alh /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release/
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05cl_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05tucl_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05tucl_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 cb05tump_ae6_aq
# drwxr-xr-x  7 roche mod3  111 Oct 17  2013 .git
# -rw-r--r--  1 roche mod3   75 Oct 17  2013 .gitignore
# -rw-r--r--  1 roche mod3 1016 Oct 17  2013 README.md
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc07tb_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc07tc_ae6_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc99_ae5_aq
# drwxr-xr-x  2 roche mod3 4.0K Oct 17  2013 saprc99_ae6_aq
# drwxr-xr-x  2 roche mod3   35 Oct 17  2013 trac0
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release -type d | fgrep -ve '.git' | wc -l
# 10
# $ find /project/inf35w/roche/CMAQ-5.0.1/git/test/MECHS-release -type f | fgrep -ve '.git' | wc -l
# 82

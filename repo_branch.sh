#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Just a utility to "simultaneously" branch all the CMAQ-build repos: NOT part of the build/run.
### See also the very similar ./repo_scan.sh. TODO: refactor.

THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # use for REPO_ROOT below
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### branch name *must* be supplied by caller
if [[ -z "${BRANCH_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} BRANCH_NAME not defined, exiting ..."
  exit 1
fi

### repo root *can* be supplied by caller
if [[ -z "${REPO_ROOT}" ]] ; then
  ## assume it's the parent of the dir of this script
  REPO_ROOT="$(dirname ${THIS_DIR})"
  if [[ -z "${REPO_ROOT}" ]] ; then
    echo -e "${ERROR_PREFIX} could not define REPO_ROOT, exiting ..."
    exit 2
  fi
fi

REPO_CMD="git branch ${BRANCH_NAME}" # just branch
# REPO_CMD="git checkout -b ${BRANCH_NAME}" # branch and switch to it

# ASSERT: same as repo_scan.sh from here out
for REPO_DIR in $(find ${REPO_ROOT}/ -maxdepth 1 -type d | grep -ve '/$' | grep -ve 'BLD_\|RUN_' | sort) ; do
#  echo -e "REPO_DIR='${REPO_DIR}'"
  for CMD in \
    "pushd ${REPO_DIR}" \
    "${REPO_CMD}" \
    'popd' \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
  echo # newline
done

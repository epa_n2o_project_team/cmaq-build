#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Create git repos from CMAQ-5.0.1 tarballs (and files in this repo)
### Requires from this repository
### * get_one_way_tarballs.sh: get and export code from `cvs`-based CMAQ-5.0.1 tarballs
### * one_way_tarsplat_to_git.sh: create `git` repos from the previous and files in this repo
### Assumes platform=linux.

### TODO: add logging, test for failure(s) in payload

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS_FP="$0"
THIS_DIR="$(dirname ${THIS_FP})"
THIS_FN="$(basename ${THIS_FP})"
export ERROR_PREFIX="${THIS_FN}: ERROR: "

TARBALL_HELPER="${THIS_DIR}/get_one_way_tarballs.sh"
export CMAQ_VERSION='5.0.1'
CMAQ_STAMP="CMAQv${CMAQ_VERSION}"
# export DOWNLOAD_ROOT="/tmp/${CMAQ_STAMP}"                        # on terrae
DOWNLOAD_ROOT='/project/inf35w/roche/CMAQ-5.0.1' # on infinity
export TARBALL_DIR="${DOWNLOAD_ROOT}/tarballs"
export TARSPLAT_ROOT="${DOWNLOAD_ROOT}/tarsplat-transplant"
export TARSPLAT_DIR="${TARSPLAT_ROOT}/${CMAQ_STAMP}"

TARSPLAT_HELPER="${THIS_DIR}/one_way_tarsplat_to_git.sh"
export CVS_TAG="${CMAQ_STAMP//./_}" # used in tarball CVS repos
export REPO_ROOT="${DOWNLOAD_ROOT}/git/test"
export BUILDER_REPO_DIR="${THIS_DIR}"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### test for
### * required inputs
### * outputs already produced

if [[ -z "${REPO_ROOT}" ]] ; then
  echo -e "${ERROR_PREFIX} undefined REPO_ROOT, exiting."
  exit 1
elif [[ ! -d "${REPO_ROOT}" ]] ; then
  for CMD in \
    "mkdir -p ${REPO_ROOT}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

## if these repos exist, there's no point in running this script
for REPO_NAME in \
  'BCON' \
  'BLDMAKE' \
  'CCTM' \
  'ICON' \
  'ICL' \
  'JPROC' \
  'MECHS' \
  'PARIO' \
  'PROCAN' \
  'STENEX' \
  'TOOLS' \
; do
  REPO_DIR="${REPO_ROOT}/${REPO_NAME}"
  if [[ -d "${REPO_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} move or delete REPO_DIR='${REPO_DIR}', exiting."
    exit 2
  fi
done

if [[ -z "${TARBALL_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} undefined TARBALL_DIR, exiting."
  exit 3
elif [[ ! -d "${TARBALL_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${TARBALL_DIR}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
fi

if [[ -z "${TARSPLAT_ROOT}" ]] ; then
  echo -e "${ERROR_PREFIX} undefined TARSPLAT_ROOT, exiting."
  exit 1
fi

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

#  "${TARBALL_HELPER}" \  # download/explode tarballs, producing tarsplat
#  "${TARSPLAT_HELPER}" \ # convert tarsplat into git repos
for CMD in \
  "${TARBALL_HELPER}" \
  "${TARSPLAT_HELPER}" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done
exit 0 # success!

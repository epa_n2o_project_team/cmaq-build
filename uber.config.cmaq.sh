#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Consistently set environment variables for config.cmaq, that maybe should be set there,
### but must definitely be persisted somewhere, for reproducibility.
### TODO: keep this in sync with ./uber.config.cmaq.csh until we convert everything to `bash`

### COMPILER is actually the compiler family or vendor used throughout for building:
### can also (IIUC) represent PGI or Gnu (TODO: document those strings)
export COMPILER='intel'

### (kludge) INFINITY answers "yes" or "no" to the question "are we running on the EPA HPCC cluster"?
export INFINITY='yes'

### M3LIB is the root of the library space (for, e.g.,  netCDF, I/O API, and MPICH): see https://bitbucket.org/epa_n2o_project_team/cmaq-build
export M3LIB='/home/wdx/lib/x86_64i/intel'

### M3HOME is the root of the build space.
### CMAQ-build expects all repository directories to be one level down from this: see https://bitbucket.org/epa_n2o_project_team/cmaq-build
export M3HOME='/project/inf35w/roche/CMAQ-5.0.1/git/old'

### M3MODEL is what we want to build. Below points to repo=CCTM, can be overridden.
export M3MODEL="${M3HOME}/CCTM"

### M3DATA is the root of the data space.
### For the CMAQ-5.0.1 benchmark, point to the .../CMAQv5.0.1/data/ in the tarsplat.
export M3DATA='/project/inf35w/roche/CMAQ-5.0.1/tarsplat/CMAQv5.0.1/data'

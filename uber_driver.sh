#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### This script runs code either

### * cloned from the `git` repositories associated with
###   https://bitbucket.org/epa_n2o_project_team/cmaq_bc_ic_plus_n2o
###   You probably wanna check its README before running this.

### * (for code you haven't committed) copied from filesystem with root directory/folder=SOURCE_ROOT (below).

### If you decide to run this, you should first

### * un/comment var=ACCESS to reflect your available/desired file-transfer protocol

### * edit var=NEW_M3HOME to state where to put the code you want to run.
###   All repos (or copies thereof) will be placed in dirs/folders one level down from that.

### and run sorta like this example:

### NEW_M3HOME='/tmp/cmaq-build' ; START="$(date)" ; echo -e "START=${START}" ; rm -fr ${NEW_M3HOME}/ ; ./uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

### Note: this script expects to be run on a linux (tested with RHEL5), and requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!) recent enough for string-replacement syntax
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `rsync`
### * `date`
### * `dirname`
### * `readlink`
### * `sed`
### * `sort`
### * `tee`
### * `tr`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### How (or wherefrom) to put our code-to-test?
### Modes of access to sources (see setup_sources): either copy, or clone from repos:
ACCESS='file'    # copy code from filesystem rooted @ same dir/folder as this script
# ACCESS='repo'    # clone all code from repositories

### Where to put our code-to-test?
### Whether copied or cloned: in dirs/folders underneath root=NEW_M3HOME
NEW_M3HOME='/tmp/cmaq-build'

### logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
export LOG_FP="${THIS_DIR}/${LOG_FN}"

### more constants and other envvars setup below in 'function setup_vars'

### common code for build and run ...
UTILS="${THIS_DIR}/CMAQ_utilities.sh"
### ... excepting this, which definition doesn't work in either CMAQ_utilities.sh or 'function setup_vars' below
### How to run our code-to-test, once we put it where we put it?
### Put it in the same place relative to NEW_M3HOME
DRIVER_FN='CMAQ-build_driver.sh'    # TODO: allow setting from commandline
export DRIVER_FP="${NEW_M3HOME}/CMAQ-build/${DRIVER_FN}"

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'setup_vars' \
    'setup_sources' \
    'setup_resources' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
# this fails: vars don't get set
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 11
    fi
  done # for CMD
} # function setup

### Set main/pilot envvars from our uber.config.cmaq
function setup_vars {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # common code for build and run in UTILS::setup_CMAQ_common_vars
  if [[ -r "${UTILS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} about to 'source' utilities file='${UTILS}'" 2>&1 | tee -a "${LOG_FP}"
    source "${UTILS}"
  else
    echo -e "${ERROR_PREFIX} cannot read utilities file='${UTILS}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi

  for CMD in \
    'setup_CMAQ_common_vars' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
# this fails: vars don't get set
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 7
    fi
  done

  echo -e "${MESSAGE_PREFIX} startup M3HOME='${M3HOME}', NEW_M3HOME='${NEW_M3HOME}'" 2>&1 | tee -a "${LOG_FP}"

  ### React to choice of mode=ACCESS
  if   [[ -z "${ACCESS}" ]] ; then
    echo -e "${ERROR_PREFIX} mode ACCESS is not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ "${ACCESS}" == 'file' ]] ; then
  #  SOURCE_ROOT="${THIS_DIR}/.."
  #  SOURCE_ROOT="$(readlink -f ${THIS_DIR}/..)" # root is one level up
    export SOURCE_ROOT="${M3HOME}" # for this code, not code-to-test
  elif [[ "${ACCESS}" == 'repo' ]] ; then
    CLONER_FN='clone_repos.sh'
    export CLONER_FP="${THIS_DIR}/${CLONER_FN}"
  else
    echo -e "${ERROR_PREFIX} unknown mode ACCESS='${ACCESS}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 6
  fi # if   [[ -z "${ACCESS}" ]]

} # function setup_vars

function setup_sources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local SOURCE_DIR='' # used below
  local SOURCE_DIR_NAME='' # used below
  local TARGET_DIR='' # used below

  if   [[ -z "${ACCESS}" ]] ; then
    echo -e "${ERROR_PREFIX} mode ACCESS is not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ "${ACCESS}" == 'file' ]] ; then

    for CMD in \
      "mkdir -p ${NEW_M3HOME}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS})\n" 2>&1 | tee -a "${LOG_FP}"
        exit 12
      fi
    done

#    echo -e "\n${MESSAGE_PREFIX} about to copy source dirs from '${SOURCE_ROOT}/'\n" 2>&1 | tee -a "${LOG_FP}" # debugging

    ### Don't copy ${M3HOME}/{BLD_*,RUN_*} dirs by default: build everything
    ### TODO: make arg(s) to switch this: would be useful for testing
    for SOURCE_DIR in $(find "${SOURCE_ROOT}/" -maxdepth 1 -type d | grep -ve '/$' | sort) ; do
      SOURCE_DIR_NAME="$(basename ${SOURCE_DIR})"
      if   [[ "${SOURCE_DIR_NAME#BLD_}" !=  "${SOURCE_DIR_NAME}" ]] ; then
        echo -e "${MESSAGE_PREFIX} skipping old build dir='${SOURCE_DIR}'"
      elif [[ "${SOURCE_DIR_NAME#RUN_}" !=  "${SOURCE_DIR_NAME}" ]] ; then
        echo -e "${MESSAGE_PREFIX} skipping old run dir='${SOURCE_DIR}'"
      else
        TARGET_DIR="${NEW_M3HOME}/${SOURCE_DIR_NAME}"
#          "cp -r ${SOURCE_DIR}/* ${TARGET_DIR}" \ # doesn't copy .../.git/ !
        for CMD in \
          "mkdir -p ${TARGET_DIR}" \
          "rsync -aq ${SOURCE_DIR}/ ${TARGET_DIR}/" \
        ; do
          echo -e "$ ${CMD}"
          eval "${CMD}"
          if [[ $? -ne 0 ]] ; then
            echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS})\n" 2>&1 | tee -a "${LOG_FP}"
            exit 12
          fi
        done # for CMD
      fi # if   [[ "${SOURCE_DIR_NAME
      echo # newline
    done # for SOURCE_DIR_NAME

  elif [[ "${ACCESS}" == 'repo' ]] ; then # we're cloning

#      "mkdir -p ${NEW_M3HOME}" # let cloner make its own
# TODO: gotta call DRIVER_FP after cloning!
    for CMD in \
      "${CLONER_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
          echo -e "${ERROR_PREFIX} '${CMD}' failed (with ACCESS==${ACCESS})\n" 2>&1 | tee -a "${LOG_FP}"
          exit 13
      fi
    done

  else
    echo -e "${ERROR_PREFIX} cannot handle access mode='${ACCESS}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  fi # if   [[ -z "${ACCESS}" ]]

  ## but gotta fix M3HOME in code-to-test
  if   [[ ! -x "${DRIVER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} build driver='${DRIVER_FP}' not executable (with ACCESS==${ACCESS})" 2>&1 | tee -a "${LOG_FP}"
    exit 14
  elif [[ ! -w "${DRIVER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} build driver='${DRIVER_FP}' not writable (with ACCESS==${ACCESS})" 2>&1 | tee -a "${LOG_FP}"
    exit 15
  else

    ## First, override M3HOME in the *new* (copied or cloned) uber.config.

#    echo -e "\n${MESSAGE_PREFIX} UBER_CONFIG_CMAQ_PATH='${UBER_CONFIG_CMAQ_PATH}', M3HOME='${M3HOME}', NEW_M3HOME='${NEW_M3HOME}'\n" 2>&1 | tee -a "${LOG_FP}" # debugging
#    NEWBER_CONFIG_CMAQ_PATH="$(echo -e ${UBER_CONFIG_CMAQ_PATH} | sed -e 's%${M3HOME}%${NEW_M3HOME}%')"
# fails with single quotes :-( bash can be a pain sometimes ...
    NEWBER_CONFIG_CMAQ_PATH="$(echo -e ${UBER_CONFIG_CMAQ_PATH} | sed -e s%${M3HOME}%${NEW_M3HOME}%)"
#    echo -e "\n${MESSAGE_PREFIX} NEWBER_CONFIG_CMAQ_PATH='${NEWBER_CONFIG_CMAQ_PATH}'\n" 2>&1 | tee -a "${LOG_FP}" # debugging

    echo -e "${MESSAGE_PREFIX} overriding M3HOME in build uber-config='${NEWBER_CONFIG_CMAQ_PATH}' with" 2>&1 | tee -a "${LOG_FP}"
    # don't edit the following line! remember, this is not going to screen/log, it's going to uber.config.cmaq.*
    echo -e "\n# override from uber_driver.sh:\nexport M3HOME='${NEW_M3HOME}'\n" >> "${NEWBER_CONFIG_CMAQ_PATH}"

    ## Second, in the main loop, we'll clear M3HOME et al, to force the new/copied/cloned driver to re-read them from uber.config.
    ## But we can't do that here, since we need these values for setup_resources.

    for CMD in \
      "tail -n 3 ${NEWBER_CONFIG_CMAQ_PATH}" \
    ; do
      echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${LOG_FP}"
        exit 12
      fi
    done # for CMD
  fi
} # end function setup_sources

### Copy non-source materials needed, e.g., for testing.
function setup_resources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  local SOURCE_DIR="${M3HOME}/${GOLD_DIR_NAME}"

  for CMD in \
    "setup_BCON_resources" \
    "setup_ICON_resources" \
  ; do
    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${LOG_FP}"
      exit 12
    fi
  done # for CMD
} # end function setup_resources

### Note setup_BCON_resources == (setup_ICON_resources ~= s/icon/bcon/g). TODO: refactor!
function setup_BCON_resources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  local TARGET_DIR="$(echo -e ${BCON_GOLD_DIR}  | sed -e s%${M3HOME}%${NEW_M3HOME}%)"

  for CMD in \
    "mkdir -p ${TARGET_DIR}/" \
    "rsync -aq ${BCON_GOLD_DIR}/ ${TARGET_DIR}/" \
  ; do
    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${LOG_FP}"
      exit 12
    fi
  done # for CMD
} # end function setup_BCON_resources

### Note setup_ICON_resources == (setup_BCON_resources ~= s/icon/icon/g). TODO: refactor!
function setup_ICON_resources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  local TARGET_DIR="$(echo -e ${ICON_GOLD_DIR}  | sed -e s%${M3HOME}%${NEW_M3HOME}%)"

  for CMD in \
    "mkdir -p ${TARGET_DIR}/" \
    "rsync -aq ${ICON_GOLD_DIR}/ ${TARGET_DIR}/" \
  ; do
    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${LOG_FP}"
      exit 12
    fi
  done # for CMD
} # end function setup_ICON_resources

### Analyze output.
function inspect {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  echo -e "\n${MESSAGE_PREFIX} deferring to CMAQ-build_driver.sh::inspect\n" 2>&1 | tee -a "${LOG_FP}"

} # end function inspect

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Main loop is, or was:

#  'setup' \ # sources, resources
#  "${DRIVER_FP}" \ # build, run
#  'inspect' \ # what have we done?
#  'teardown' # as needed

### However I gotta break it to force new DRIVER_FP to reread its uber.config (as discussed in function=setup_sources):

for CMD in \
  'setup' \
; do
  if [[ -z "${CMD}" ]] ; then
    echo -e "${THIS_FN}::main loop: ERROR: CMD not defined, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
    exit 18
  else
    echo -e "\n$ ${THIS_FN}::main loop::${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
#   this fails: vars (e.g., M3HOME) don't get exported (e.g., to function=inspect_all_output)
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
#   this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::main loop::${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 18
    fi
  fi # if [[ -z "${CMD}" ]]
done

### Now clear M3HOME (et al?) to force new DRIVER_FP to reread its uber.config
export M3HOME=''

for CMD in \
  "${DRIVER_FP}" \
  'inspect' \
; do
  if [[ -z "${CMD}" ]] ; then
    echo -e "${THIS_FN}::main loop: ERROR: CMD not defined, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
    exit 18
  else
    echo -e "\n$ ${THIS_FN}::main loop::${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
#    eval "${CMD}"
# no vars get set above (IIRC), so ...
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::main loop::${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 18
    fi
  fi # if [[ -z "${CMD}" ]]
done

exit 0 # success!
